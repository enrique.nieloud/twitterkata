//
//  CommonStructs.swift
//  TwitterKataREST
//
//  Created by Enrique Nieloud on 26/08/2021.
//

import Foundation

struct NickNameDTO: Codable {
    let nickName: String
}

struct TweetDTO: Codable {
    let nickName: String
    let tweetMessage: String
    init(from tweet: Tweet) {
        self.nickName = tweet.nickName
        self.tweetMessage = tweet.message
    }
    func toCreateTweetData() -> CreateTweetData {
        return CreateTweetData(nickName: self.nickName, tweetMessage: self.tweetMessage)
    }
}

struct FollowDTO: Codable {
    let fromNickName: String
    let toNickName: String
}

struct UserDTO: Codable {
    let nickName: String
    let realName: String
}

struct UpdateRealNameDTO: Codable {
    let nickName: String
    let updatedRealName: String
}

