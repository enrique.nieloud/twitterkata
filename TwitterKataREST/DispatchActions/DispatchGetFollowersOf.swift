//
//  DispatchGetFollowersOf.swift
//  TwitterKataREST
//
//  Created by Enrique Nieloud on 19/08/2021.
//

import Foundation

class DispatchGetFollowersOf: DispatchHttpRequestBase {
    var ok = true
    var followers: [String] = []
    let getFollowersOfAction: GetFollowersOf

    init(followService: FollowService) {
        getFollowersOfAction = GetFollowersOfDefault(followService: followService)
        super.init()
        self.verb = "GET"
        self.route = "/followers/get"
    }
    
    override func getCustomResponse() -> HttpResponse {
        let jsonData = try! JSONEncoder().encode(followers)
        let jsonObject = try! JSONSerialization.jsonObject(with: jsonData, options: [])
        return HttpResponse.ok(.json(jsonObject))
    }
    
    private func callGetFollowersOfAction(nickName: String) {
        do {
            followers = try getFollowersOfAction(nickName: nickName)
            actionResponse = ActionResponse(ok: true, message: "")
        } catch let error as TwitterKataError {
            actionResponse = ActionResponse(ok: false, message: error.description )
        } catch {
            actionResponse = ActionResponse(ok: false, message: error.localizedDescription)
        }
    }
    
    override func executeAction(request: HttpRequest) {
        do {
            let follow = try JSONDecoder().decode(NickNameDTO.self, from: Data(request.body))
            callGetFollowersOfAction(nickName: follow.nickName)
        } catch _ as DecodingError {
            actionResponse = ActionResponse(ok: false, message: "Invalid JSON format")
        } catch {
            actionResponse = ActionResponse(ok: false, message: error.localizedDescription)
        }
    }
}
