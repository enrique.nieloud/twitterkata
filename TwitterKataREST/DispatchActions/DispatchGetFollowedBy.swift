//
//  DispatchGetFollowedBy.swift
//  TwitterKataREST
//
//  Created by Enrique Nieloud on 19/08/2021.
//

import Foundation

class DispatchGetFollowedBy: DispatchHttpRequestBase {
    var ok = true
    var followed: [String] = []
    let getFollowedByAction: GetFollowedBy

    init(followService: FollowService) {
        getFollowedByAction = GetFollowedByDefault(followService: followService)
        super.init()
        self.verb = "GET"
        self.route = "/followed/get"
    }
    
    override func getCustomResponse() -> HttpResponse {
        let jsonData = try! JSONEncoder().encode(followed)
        let jsonObject = try! JSONSerialization.jsonObject(with: jsonData, options: [])
        return HttpResponse.ok(.json(jsonObject))
    }
    
    private func callGetFollowedByAction(nickName: String) {
        do {
            followed = try getFollowedByAction(nickName: nickName)
            actionResponse = ActionResponse(ok: true, message: "")
        } catch let error as TwitterKataError {
            actionResponse = ActionResponse(ok: false, message: error.description )
        } catch {
            actionResponse = ActionResponse(ok: false, message: error.localizedDescription)
        }
    }
    
    override func executeAction(request: HttpRequest) {
        do {
            let follow = try JSONDecoder().decode(NickNameDTO.self, from: Data(request.body))
            callGetFollowedByAction(nickName: follow.nickName)
        } catch _ as DecodingError {
            actionResponse = ActionResponse(ok: false, message: "Invalid JSON format")
        } catch {
            actionResponse = ActionResponse(ok: false, message: error.localizedDescription)
        }
    }
}
