//
//  CreateUserProcessor.swift
//  TwitterKataREST
//
//  Created by Enrique Nieloud on 19/08/2021.
//

import Foundation

class DispatchCreateUser: DispatchHttpRequestBase {
    var ok = true
    let registerUserAction: RegisterUser

    init(userService: UserService) {
        registerUserAction = RegisterUserDefault(userService: userService)
        super.init()
        self.verb = "POST"
        self.route = "/user/create"
    }
    
    private func callRegisterAction(nickName: String, realName: String) {
        do {
            try registerUserAction(nickName: nickName, realName: realName)
            actionResponse = ActionResponse(ok: true, message: "User \(nickName) successfully created")
        } catch let error as TwitterKataError {
            actionResponse = ActionResponse(ok: false, message: error.description )
        } catch {
            actionResponse = ActionResponse(ok: false, message: error.localizedDescription)
        }
    }
    
    override func executeAction(request: HttpRequest) {
        do {
            let user = try JSONDecoder().decode(UserDTO.self, from: Data(request.body))
            callRegisterAction(nickName: user.nickName, realName: user.realName)
        } catch _ as DecodingError {
            actionResponse = ActionResponse(ok: false, message: "Invalid JSON format")
        } catch {
            actionResponse = ActionResponse(ok: false, message: error.localizedDescription)
        }
    }
}
