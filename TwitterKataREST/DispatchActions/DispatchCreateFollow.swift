//
//  DispatchCreateFollow.swift
//  TwitterKataREST
//
//  Created by Enrique Nieloud on 19/08/2021.
//

import Foundation

class DispatchCreateFollow: DispatchHttpRequestBase {
    var ok = true
    let createFollowAction: CreateFollowDefault

    init(followService: FollowService) {
        createFollowAction = CreateFollowDefault(followService: followService)
        super.init()
        self.verb = "POST"
        self.route = "/follow/create"
    }
    
    private func callCreateFollowAction(fromNickName: String, toNickName: String) {
        do {
            try createFollowAction(from: fromNickName, to: toNickName)
            actionResponse = ActionResponse(ok: true, message: "Follow from \(fromNickName) to \(toNickName) successfully created")
        } catch let error as TwitterKataError {
            actionResponse = ActionResponse(ok: false, message: error.description )
        } catch {
            actionResponse = ActionResponse(ok: false, message: error.localizedDescription)
        }
    }
    
    override func executeAction(request: HttpRequest) {
        do {
            let follow = try JSONDecoder().decode(FollowDTO.self, from: Data(request.body))
            callCreateFollowAction(fromNickName: follow.fromNickName, toNickName: follow.toNickName)
        } catch _ as DecodingError {
            actionResponse = ActionResponse(ok: false, message: "Invalid JSON format")
        } catch {
            actionResponse = ActionResponse(ok: false, message: error.localizedDescription)
        }
    }
}
