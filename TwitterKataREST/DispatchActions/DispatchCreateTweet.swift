//
//  DispatchCreateTweet.swift
//  TwitterKataREST
//
//  Created by Enrique Nieloud on 26/08/2021.
//

import Foundation

class DispatchCreateTweet: DispatchHttpRequestBase {
    var ok = true
    let createTweetAction: CreateTweet

    init(tweetService: TweetService) {
        createTweetAction = CreateTweetDefault(tweetService: tweetService)
        super.init()
        self.verb = "POST"
        self.route = "/tweet/create"
    }
    
    private func callCreateTweetAction(tweet: TweetDTO) {
        do {
            try createTweetAction(createTweetData: tweet.toCreateTweetData())
            actionResponse = ActionResponse(ok: true, message: "Tweet from \(tweet.nickName) successfully created")
        } catch let error as TwitterKataError {
            actionResponse = ActionResponse(ok: false, message: error.description )
        } catch {
            actionResponse = ActionResponse(ok: false, message: error.localizedDescription)
        }
    }
    
    override func executeAction(request: HttpRequest) {
        do {
            let tweet = try JSONDecoder().decode(TweetDTO.self, from: Data(request.body))
            callCreateTweetAction(tweet: tweet)
        } catch _ as DecodingError {
            actionResponse = ActionResponse(ok: false, message: "Invalid JSON format")
        } catch {
            actionResponse = ActionResponse(ok: false, message: error.localizedDescription)
        }
    }
}
