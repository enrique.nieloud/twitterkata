//
//  DispatchGetTweets.swift
//  TwitterKataREST
//
//  Created by Enrique Nieloud on 26/08/2021.
//

import Foundation

class DispatchGetTweets: DispatchHttpRequestBase {
    var ok = true
    var tweets: [TweetDTO] = []
    let getTweetsAction: GetTweets

    init(tweetService: TweetService) {
        getTweetsAction = GetTweetsDefault(tweetService: tweetService)
        super.init()
        self.verb = "GET"
        self.route = "/tweets/get"
    }
    
    override func getCustomResponse() -> HttpResponse {
        let jsonData = try! JSONEncoder().encode(tweets)
        let jsonObject = try! JSONSerialization.jsonObject(with: jsonData, options: [])
        return HttpResponse.ok(.json(jsonObject))
    }
    
    private func callGetTweetsAction(nickName: String) {
        do {
            let tweetList = try getTweetsAction(of: nickName)
            tweets = []
            for aTweet in tweetList {
                tweets.append(TweetDTO(from: aTweet))
            }
            actionResponse = ActionResponse(ok: true, message: "")
        } catch let error as TwitterKataError {
            actionResponse = ActionResponse(ok: false, message: error.description )
        } catch {
            actionResponse = ActionResponse(ok: false, message: error.localizedDescription)
        }
    }
    
    override func executeAction(request: HttpRequest) {
        do {
            let follow = try JSONDecoder().decode(NickNameDTO.self, from: Data(request.body))
            callGetTweetsAction(nickName: follow.nickName)
        } catch _ as DecodingError {
            actionResponse = ActionResponse(ok: false, message: "Invalid JSON format")
        } catch {
            actionResponse = ActionResponse(ok: false, message: error.localizedDescription)
        }
    }
}
