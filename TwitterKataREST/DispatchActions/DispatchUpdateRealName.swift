//
//  UpdateRealNameProcessor.swift
//  TwitterKataREST
//
//  Created by Enrique Nieloud on 19/08/2021.
//

import Foundation

class DispatchUpdateRealName: DispatchHttpRequestBase {
    var ok = true
    let updateRealNameAction: UpdateRealName
    
    init(userService: UserService) {
        updateRealNameAction = UpdateRealNameDefault(userService: userService)
        super.init()
        self.verb = "PUT"
        self.route = "/user/update"
    }
    
    private func callUpdateRealNameAction(nickName: String, updatedRealName: String) {
        do {
            try updateRealNameAction(nickName: nickName, updatedRealName: updatedRealName)
            actionResponse = ActionResponse(ok: true, message: "User \(nickName) successfully updated")
        } catch TwitterKataError.nickNameNotFound {
            actionResponse = ActionResponse(ok: false, message: "User \(nickName) not found")
        } catch {
            actionResponse = ActionResponse(ok: false, message: error.localizedDescription)
        }
    }
    
    override func executeAction(request: HttpRequest) {
        do {
            let user = try JSONDecoder().decode(UpdateRealNameDTO.self, from: Data(request.body))
            callUpdateRealNameAction(nickName: user.nickName, updatedRealName: user.updatedRealName)
        } catch _ as DecodingError {
            actionResponse = ActionResponse(ok: false, message: "Invalid JSON format")
        } catch {
            actionResponse = ActionResponse(ok: false, message: error.localizedDescription)
        }
    }
}
