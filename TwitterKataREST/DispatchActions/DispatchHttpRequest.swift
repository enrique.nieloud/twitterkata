//
//  RequestProcessor.swift
//  TwitterKataREST
//
//  Created by Enrique Nieloud on 19/08/2021.
//

import Foundation

// Clase abstracta para despachar una accion en particular

struct ActionResponse : Codable {
    let ok: Bool
    let message: String
}

protocol DispatchHttpRequestProtocol {
    func dispatch(request: HttpRequest) -> HttpResponse
    var verb: String { get }
    var route: String { get }
}

class DispatchHttpRequestBase: DispatchHttpRequestProtocol {
    var verb: String = ""
    var route: String = ""
    
    var actionResponse = ActionResponse(ok: true, message: "")

    func executeAction(request: HttpRequest) {
        // override in base class
    }

    internal func getDefaultResponse() -> HttpResponse {
        let jsonData = try! JSONEncoder().encode(actionResponse)
        let jsonObject = try! JSONSerialization.jsonObject(with: jsonData, options: [])
        return HttpResponse.ok(.json(jsonObject))
    }
    
    internal func getCustomResponse() -> HttpResponse {
        getDefaultResponse()
    }
    
    internal func getResponse() -> HttpResponse {
        if actionResponse.ok == false {
            return getDefaultResponse()
        } else {
            return getCustomResponse()
        }
    }
    
    func dispatch(request: HttpRequest) -> HttpResponse {
        actionResponse = ActionResponse(ok: true, message: "")
        if request.method == verb {
            executeAction(request: request)
        } else {
            actionResponse = ActionResponse(ok: false, message: "Invalid verb:\(request.method) should be: \(verb)")
        }
        return getResponse()
    }
}
