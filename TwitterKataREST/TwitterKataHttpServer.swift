//
//  TwitterKataHttpServer.swift
//  TwitterKataREST
//
//  Created by Enrique Nieloud on 19/08/2021.
//

import Foundation

// Atiende las peticiones HTTP / REST y las despacha a las correspondientes acciones

class TwitterKataHttpServer {
    let server = HttpServer()

    var dispatchers: [DispatchHttpRequestProtocol] = []
    
    func setupRoutes() throws {
        let dbName = "db.sqlite"
        let userRepository = try SqliteUserRepository(dbName: dbName)
        let userService = UserServiceDefault(userRepository: userRepository)
        let tweetIdGenerator = try SqliteTweetIdGenerator(dbName: dbName)
        let tweetRepository = try SqliteTweetRepository(dbName: dbName)
        let tweetService = TweetServiceDefault(tweetIdGenerator: tweetIdGenerator, tweetRepository: tweetRepository, userService: userService)
        let followRepository = try SqliteFollowRepository(dbName: dbName)
        let followService = FollowServiceDefault(followRepository: followRepository, userService: userService)
        dispatchers = [
            DispatchCreateUser(userService: userService),
            DispatchUpdateRealName(userService: userService),
            DispatchCreateFollow(followService: followService),
            DispatchGetFollowersOf(followService: followService),
            DispatchGetFollowedBy(followService: followService),
            DispatchCreateTweet(tweetService: tweetService),
            DispatchGetTweets(tweetService: tweetService),
        ]
    }
    
    func prepareRoutesHandlers() {
        for dipatcher in dispatchers {
            server[dipatcher.route] = { request in
                return dipatcher.dispatch(request: request)
            }
        }
    }
    
    init() throws {
        try setupRoutes()
        prepareRoutesHandlers()
    }
    
    func start() throws {
        try server.start()
    }
}
