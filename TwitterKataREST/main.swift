//
//  main.swift
//  TwitterKataREST
//
//  Created by Enrique Nieloud on 18/08/2021.
//

import Foundation

let twitterKataHttpServer = try TwitterKataHttpServer()
try twitterKataHttpServer.start()
RunLoop.main.run()
