//
//  InMemoryTweetRepository.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 25/08/2021.
//

import Foundation
import SQLite

class SqliteTweetRepository: SqliteRepository, TweetRepository {
    let tweets = Table("tweets")
    let id = Expression<Int64>("id")
    let date = Expression<Date>("date")
    let nickName = Expression<String>("nickName")
    let message = Expression<String>("message")

    override func setupDB() throws {
        try db.run(tweets.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(date)
            t.column(nickName)
            t.column(message)
        })
    }
    
    func findTweets(of nickName: String) throws -> [Tweet] {
        var tweetsFound = [Tweet]()
        for row in try db.prepare(tweets.where(self.nickName == nickName)) {
            tweetsFound.append(DbRowToTweet(tweetRow: row))
        }
        return tweetsFound
    }
    
    func save(tweet: Tweet) throws {
        if exists(id: tweet.id) {
            throw TwitterKataError.tweetAlreadyExists
        }
        try db.transaction {
            try db.run(tweets.insert(
                        id <- Int64(tweet.id),
                        date <- tweet.date,
                        nickName <- tweet.nickName,
                        message <- tweet.message))
        }
    }

    func exists(id: TweetID) -> Bool {
        do {
            let count = try db.scalar(tweets.where(self.id == Int64(id)).count)
            return count > 0
        } catch {
            return false
        }
    }

    func DbRowToTweet(tweetRow: Row) -> Tweet {
        let tweetId = TweetID(tweetRow[self.id])
        let tweetDate = tweetRow[self.date]
        let tweetNickName = tweetRow[self.nickName]
        let tweetMessage = tweetRow[self.message]
        return Tweet(id: tweetId, date: tweetDate , nickName: tweetNickName, message: tweetMessage)
    }
}
