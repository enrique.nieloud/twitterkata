//
//  InMemoryTweetRepository.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 25/08/2021.
//

import Foundation

class InMemoryTweetRepository: TweetRepository {
    var tweets: [Tweet] = []
    
    func findTweets(of nickName: String) throws -> [Tweet] {
        return tweets.filter({ $0.nickName == nickName })
    }
    
    func save(tweet: Tweet) throws {
        if exists(id: tweet.id) {
            throw TwitterKataError.tweetAlreadyExists
        } else {
            tweets.append(tweet)
        }
    }

    func exists(id: TweetID) -> Bool {
        return tweets.firstIndex(where: { $0.id == id }) != nil
    }
}
