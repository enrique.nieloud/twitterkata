//
//  SqliteTweetIdGenerator.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 25/08/2021.
//

import Foundation
import SQLite

class SqliteTweetIdGenerator: SqliteRepository, TweetIdGenerator {
    
    let tweetSequenceName = "tweetSequence"
    let sequence = Table("sequence")
    let sequenceName = Expression<String>("sequenceName")
    let sequenceNumber = Expression<Int64>("sequenceNumber")
    
    func callAsFunction() throws -> TweetID  {
        for sec in try db.prepare(sequence.where(sequenceName == tweetSequenceName)) {
            let sequenceNumber = TweetID(sec[sequenceNumber])
            try updateTweetSequenceNumber()
            return sequenceNumber
        }
        throw TwitterKataError.sequenceNumberNotFound
    }
    
    override func setupDB() throws {
        try db.run(sequence.create(ifNotExists: true) { t in
            t.column(sequenceName, primaryKey: true)
            t.column(sequenceNumber)
        })
        try initializeTweetSequenceNumber()
    }
    
    func isTweetSequenceNumberCreated() -> Bool {
        do {
            let count = try db.scalar(sequence.where(sequenceName == tweetSequenceName).count)
            return count > 0
        } catch {
            return false
        }
    }
    
    func initializeTweetSequenceNumber() throws {
        if isTweetSequenceNumberCreated() {
            return
        }
        try db.transaction {
            try db.run(sequence.insert(sequenceName <- tweetSequenceName, sequenceNumber <- 0))
        }
    }
    
    func updateTweetSequenceNumber() throws {
        if !isTweetSequenceNumberCreated() {
            throw TwitterKataError.sequenceNumberNotFound
        }
        try db.transaction {
            let sequenceRow = sequence.where(sequenceName == tweetSequenceName)
            try db.run(sequenceRow.update(sequenceNumber <- sequenceRow[sequenceNumber] + 1))
        }
    }
}
