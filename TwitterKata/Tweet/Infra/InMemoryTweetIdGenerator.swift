//
//  InMemoryTweetIdGenerator.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 24/08/2021.
//

import Foundation

class InMemoryTweetIdGenerator: TweetIdGenerator {
    var id: Int64 = 0
    
    func callAsFunction() throws -> TweetID {
        id += 1
        return TweetID(id)
    }
}
