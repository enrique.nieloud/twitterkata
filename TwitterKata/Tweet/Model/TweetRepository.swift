//
//  TweetRepository.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 24/08/2021.
//

import Foundation

//sourcery: AutoMockable
protocol TweetRepository {
    func save(tweet: Tweet) throws
    func findTweets(of nickName: String) throws -> [Tweet]
}
