//
//  TweetIdGenerator.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 24/08/2021.
//

import Foundation

typealias TweetID = UInt64

//sourcery: AutoMockable
protocol TweetIdGenerator {
    func callAsFunction() throws -> TweetID
}
