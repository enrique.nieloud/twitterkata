//
//  Tweet.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 24/08/2021.
//

import Foundation

struct Tweet: Equatable {
    let id: TweetID
    let date: Date
    let nickName: String
    let message: String
    static func == (lhs: Tweet, rhs: Tweet) -> Bool {
        return
            lhs.id == rhs.id &&
            abs(lhs.date - rhs.date) < 0.01 &&
            lhs.nickName == rhs.nickName &&
            lhs.message == rhs.message
    }
}

