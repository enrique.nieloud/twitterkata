//
//  TweetService.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 24/08/2021.
//

import Foundation

//sourcery: AutoMockable
protocol TweetService {
    func createTweet(createTweetData: CreateTweetData) throws
    func getTweets(of nickName: String) throws -> [Tweet]
}

class TweetServiceDefault : TweetService {
    let idGenerator: TweetIdGenerator
    let repository: TweetRepository
    let userService: UserService

    init(tweetIdGenerator: TweetIdGenerator, tweetRepository: TweetRepository, userService: UserService) {
        self.idGenerator = tweetIdGenerator
        self.repository = tweetRepository
        self.userService = userService
    }

    func getTweets(of nickName: String) throws  -> [Tweet] {
        if nickName.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            throw TwitterKataError.nickNameMustNotBeEmpty
        }
        if !userService.exists(nickName: nickName) {
            throw TwitterKataError.nickNameNotFound
        }
        return try repository.findTweets(of: nickName)
    }
    
    func createTweet(createTweetData: CreateTweetData) throws {
        if createTweetData.nickName.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            throw TwitterKataError.nickNameMustNotBeEmpty
        }
        if createTweetData.tweetMessage.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            throw TwitterKataError.tweetMessageMustNotBeEmpty
        }
        if !userService.exists(nickName: createTweetData.nickName) {
            throw TwitterKataError.nickNameNotFound
        }
        let tweet = Tweet(id: try idGenerator(), date: Date() , nickName: createTweetData.nickName, message: createTweetData.tweetMessage)
        try repository.save(tweet: tweet)
    }
}
