//
//  CreateTweet.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 24/08/2021.
//

import Foundation

struct CreateTweetData: Equatable {
    let nickName: String
    let tweetMessage: String
}

protocol CreateTweet {
    func callAsFunction(createTweetData: CreateTweetData) throws
}

class CreateTweetDefault: CreateTweet {
    private let tweetService: TweetService
    
    init(tweetService: TweetService) {
        self.tweetService = tweetService
    }

    func callAsFunction(createTweetData: CreateTweetData) throws {
        try tweetService.createTweet(createTweetData: createTweetData)
    }
}
