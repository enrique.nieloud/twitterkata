//
//  GetTweets.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 24/08/2021.
//

import Foundation

protocol GetTweets {
    func callAsFunction(of nickName: String) throws -> [Tweet]
}

class GetTweetsDefault: GetTweets {

    private let tweetService: TweetService
    
    init(tweetService: TweetService) {
        self.tweetService = tweetService
    }

    func callAsFunction(of nickName: String) throws -> [Tweet] {
        return try tweetService.getTweets(of: nickName)
    }
}
