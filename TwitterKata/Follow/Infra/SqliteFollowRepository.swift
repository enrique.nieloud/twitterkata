//
//  SqliteFollowRepository.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 23/08/2021.
//

import Foundation
import SQLite

class SqliteFollowRepository: SqliteRepository, FollowRepository {
    
    let follows = Table("follows")
    let fromNickName = Expression<String>("fromNickName")
    let toNickName = Expression<String>("toNickName")

    override func setupDB() throws {
        try db.run(follows.create(ifNotExists: true) { t in
            t.column(fromNickName)
            t.column(toNickName)
            t.primaryKey(fromNickName, toNickName)
        })
    }
    
    func findFollowers(of nickName: String) throws -> [String] {
        var followersFound: [String] = []
        for follow in try db.prepare(follows.where(toNickName == nickName)) {
            followersFound.append(follow[fromNickName])
        }
        return followersFound
    }
    
    func findFollowed(by nickName: String) throws -> [String] {
        var followedFound: [String] = []
        for follow in try db.prepare(follows.where(fromNickName == nickName)) {
            followedFound.append(follow[toNickName])
        }
        return followedFound
    }
    
    func exists(follow: Follow) -> Bool {
        do {
            let count = try db.scalar(follows.where(fromNickName == follow.fromNickName && toNickName == follow.toNickName).count)
            return count > 0
        } catch {
            return false
        }
    }
    
    func save(follow: Follow) throws {
        if exists(follow: follow) {
            throw TwitterKataError.followAlreadyExist
        }
        try db.transaction {
            try db.run(follows.insert(fromNickName <- follow.fromNickName, toNickName <- follow.toNickName))
        }
    }
}
