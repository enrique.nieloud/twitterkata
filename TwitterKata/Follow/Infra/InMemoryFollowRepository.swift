//
//  NonPersistentFollowRepository.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 12/08/2021.
//

import Foundation

class InMemoryFollowRepository: FollowRepository {
    var follows = [Follow]()

    func findFollowers(of nickName: String) throws -> [String] {
        return follows.filter({$0.toNickName == nickName }).map({$0.fromNickName})
    }
    
    func findFollowed(by nickName: String) throws -> [String] {
        return follows.filter({$0.fromNickName == nickName }).map({$0.toNickName})
    }
    
    func save(follow: Follow) throws {
        if follows.contains(where: {$0 == follow}) {
            throw TwitterKataError.followAlreadyExist
        }
        follows.append(follow)
    }
}
