//
//  GetFollowedBy.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 17/08/2021.
//

import Foundation

protocol GetFollowedBy {
    func callAsFunction(nickName: String) throws -> [String]
}

class GetFollowedByDefault: GetFollowedBy {
    private let followService: FollowService
    
    init(followService: FollowService) {
        self.followService = followService
    }

    func callAsFunction(nickName: String) throws -> [String] {
        return try followService.followed(by: nickName)
    }
}
