//
//  GetFollowersOf.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 17/08/2021.
//

import Foundation

protocol GetFollowersOf {
    func callAsFunction(nickName: String) throws -> [String]
}

class GetFollowersOfDefault: GetFollowersOf {
    private let followService: FollowService
    
    init(followService: FollowService) {
        self.followService = followService
    }

    func callAsFunction(nickName: String) throws -> [String] {
        return try followService.followers(of: nickName)
    }
}
