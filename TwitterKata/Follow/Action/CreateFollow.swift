//
//  CreateFollow.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 12/08/2021.
//

import Foundation

protocol CreateFollow {
    func callAsFunction(from fromNickName: String, to toNickName: String) throws
}

class CreateFollowDefault: CreateFollow {
    private let followService: FollowService
    
    init(followService: FollowService) {
        self.followService = followService
    }

    func callAsFunction(from fromNickName: String, to toNickName: String) throws {
        let follow = Follow(fromNickName: fromNickName, toNickName: toNickName)
        try followService.add(follow: follow)
    }
}
