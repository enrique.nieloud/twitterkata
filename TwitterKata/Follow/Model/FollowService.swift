//
//  FollowService.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 12/08/2021.
//

import Foundation

//sourcery: AutoMockable
protocol FollowService {
    func add(follow: Follow) throws
    func followers(of nickName: String) throws -> [String]
    func followed(by nickName: String) throws -> [String]
}

class FollowServiceDefault : FollowService {
    
    let followRepository: FollowRepository
    let userService: UserService
    
    init(followRepository: FollowRepository, userService: UserService) {
        self.followRepository = followRepository
        self.userService = userService
    }
    
    func add(follow: Follow) throws {
        if !userService.exists(nickName: follow.toNickName) {
            throw TwitterKataError.toNickNameNotFound
        }
        if !userService.exists(nickName: follow.fromNickName) {
            throw TwitterKataError.fromNickNameNotFound
        }
        if follow.fromNickName == follow.toNickName {
            throw TwitterKataError.aUserCannotFollowItself
        }
        try followRepository.save(follow: follow)
    }

    func followers(of nickName: String) throws -> [String] {
        return try followRepository.findFollowers(of: nickName)
    }
    
    func followed(by nickName: String) throws -> [String] {
        return try followRepository.findFollowed(by: nickName)
    }
}

