//
//  FollowRepository.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 12/08/2021.
//

import Foundation

//sourcery: AutoMockable
protocol FollowRepository {
    func save(follow: Follow) throws
    func findFollowers(of nickName: String) throws -> [String]
    func findFollowed(by nickName: String) throws -> [String]
}

