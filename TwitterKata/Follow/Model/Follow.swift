//
//  Follow.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 12/08/2021.
//

import Foundation

struct Follow: Equatable  {
    let fromNickName: String
    let toNickName: String
}
