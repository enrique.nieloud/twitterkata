//
//  RegisterUser.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 10/08/2021.
//

import Foundation

protocol RegisterUser {
    func callAsFunction(nickName: String, realName: String) throws
}

class RegisterUserDefault: RegisterUser {
    private let userService: UserService
    
    init(userService: UserService) {
        self.userService = userService
    }

    func callAsFunction(nickName: String, realName: String) throws {
        let user = User(nickName: nickName, realName: realName)
        try userService.add(user: user)
    }
}
