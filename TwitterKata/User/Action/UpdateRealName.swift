//
//  UpdateRealNameAction.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 11/08/2021.
//

import Foundation

protocol UpdateRealName {
    func callAsFunction(nickName: String, updatedRealName: String) throws
}

class UpdateRealNameDefault: UpdateRealName {
    private let userService: UserService
    
    init(userService: UserService) {
        self.userService = userService
    }

    func callAsFunction(nickName: String, updatedRealName: String) throws {
        try userService.updateRealName(nickName: nickName, updatedRealName: updatedRealName)
    }
}
