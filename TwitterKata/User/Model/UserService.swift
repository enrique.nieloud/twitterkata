//
//  UserService.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 10/08/2021.
//

import Foundation


//sourcery: AutoMockable
protocol UserService {
    func exists(nickName: String) -> Bool
    func add(user: User) throws
    func findBy(nickName: String) throws -> User
    func updateRealName(nickName: String, updatedRealName: String) throws
}

class UserServiceDefault : UserService {
    
    let userRepository: UserRepository
    
    init(userRepository: UserRepository) {
        self.userRepository = userRepository
    }
    
    func add(user: User) throws {
        if user.nickName == "" {
            throw TwitterKataError.nickNameMustNotBeEmpty
        }
        if userRepository.exists(nickName: user.nickName) {
            throw TwitterKataError.userAlreadyExits
        }
        try userRepository.save(user: user)
    }
    
    func updateRealName(nickName: String, updatedRealName: String) throws {
        let userWithRealNameChanged = User(nickName: nickName, realName: updatedRealName)
        try userRepository.update(user: userWithRealNameChanged)
    }

    func exists(nickName: String) -> Bool {
        return userRepository.exists(nickName: nickName)
    }

    func findBy(nickName: String) throws -> User {
        return try userRepository.findBy(nickName: nickName)
    }
}

