//
//  UserRepository.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 10/08/2021.
//

import Foundation

//sourcery: AutoMockable
protocol UserRepository {
    func exists(nickName: String) -> Bool
    func save(user: User) throws
    func findBy(nickName: String) throws -> User
    func update(user: User) throws
}

