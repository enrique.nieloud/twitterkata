//
//  User.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 10/08/2021.
//

import Foundation

struct User: Equatable {
    let nickName: String
    let realName: String
}
