//
//  InMemoryUserRepository.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 10/08/2021.
//

import Foundation

class InMemoryUserRepository: UserRepository {
    var users = [User]()

    func exists(nickName: String) -> Bool {
        return users.firstIndex(where: { $0.nickName == nickName }) != nil
    }
    
    func save(user: User) throws {
        if exists(nickName: user.nickName) {
            throw TwitterKataError.userAlreadyExists
        } else {
            users.append(user)
        }
    }
    
    func findBy(nickName: String) throws -> User {
        if let index = users.firstIndex(where: { $0.nickName == nickName }) {
            return users[index]
        } else {
            throw TwitterKataError.nickNameNotFound
        }
    }
    
    func update(user: User) throws {
        if let index = users.firstIndex(where: { $0.nickName == user.nickName }) {
            users[index] = user
        } else {
            throw TwitterKataError.nickNameNotFound
        }
    }
}
