//
//  SqliteUserRepository.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 20/08/2021.
//

import Foundation
import SQLite

class SqliteUserRepository: SqliteRepository, UserRepository {
    let users = Table("users")
    let nickName = Expression<String>("nickName")
    let realName = Expression<String>("realName")

    override func setupDB() throws {
        try db.run(users.create(ifNotExists: true) { t in
            t.column(nickName, primaryKey: true)
            t.column(realName)
        })
    }
    
    func exists(nickName: String) -> Bool {
        do {
            let count = try db.scalar(users.where(self.nickName == nickName).count)
            return count > 0
        } catch {
            return false
        }
    }
    
    func save(user: User) throws {
        if exists(nickName: user.nickName) {
            throw TwitterKataError.userAlreadyExists
        }
        try db.transaction {
            try db.run(users.insert(nickName <- user.nickName, realName <- user.realName))
        }
    }
    
    func findBy(nickName: String) throws -> User {
        for user in try db.prepare(users.where(self.nickName == nickName)) {
            return User(nickName: user[self.nickName], realName: user[self.realName])
        }
        throw TwitterKataError.nickNameNotFound
    }
    
    func update(user: User) throws {
        if !exists(nickName: user.nickName) {
            throw TwitterKataError.nickNameNotFound
        }
        try db.transaction {
            let userFound = users.where(nickName == nickName)
            try db.run(userFound.update(realName <- user.realName))
        }
    }
}
