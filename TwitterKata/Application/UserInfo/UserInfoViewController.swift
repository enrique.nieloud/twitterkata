//
//  UserInfoViewController.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 30/08/2021.
//

import UIKit
import RxSwift
import RxCocoa

class UserInfoViewController: UIViewController, UITableViewDelegate {
    @IBOutlet weak var tweetsTable: UITableView!
    @IBOutlet weak var followersOfTable: UITableView!
    @IBOutlet weak var followedByTable: UITableView!
    @IBOutlet weak var nickName: UITextField!
    @IBOutlet weak var nickNameFollowing: UITextField!
    @IBOutlet weak var nickNameFollowedBy: UITextField!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var getInfoButton: UIButton!
    @IBOutlet weak var addFollowerButton: UIButton!
    @IBOutlet weak var addFollowButton: UIButton!
    private var viewModel: UserInfoViewModel!
    private var disposeBag = DisposeBag()
    private let tweetsTableCellId = "tweetsTableCellId"
    private let followersOfTableCellId = "followersOfTableCellId"
    private let followedByTableCellId = "followedByTableCellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createViewModel()
        setupBindings()
        tweetsTable.register(CustomTableViewCell.self, forCellReuseIdentifier: tweetsTableCellId)
        followersOfTable.register(CustomTableViewCell.self, forCellReuseIdentifier: followersOfTableCellId)
        followedByTable.register(CustomTableViewCell.self, forCellReuseIdentifier: followedByTableCellId)
        tweetsTable.delegate = self
        followersOfTable.delegate = self
        followedByTable.delegate = self
        errorMessage.text = ""
    }

    @IBAction func getInfoExecuted(_ sender: Any) {
        viewModel.getInfo()
    }
    
    @IBAction func onAddFollowedBy(_ sender: Any) {
        viewModel.addFollowedByPressed()
    }
    
    @IBAction func onAddFollowing(_ sender: Any) {
        viewModel.addFollowingPressed()
    }
    
    @IBAction func dimissViewController(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func createViewModel() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let getTweetsAction = GetTweetsDefault(tweetService: appDelegate.tweetService)
        let createFollowAction = CreateFollowDefault(followService: appDelegate.followService)
        let getFollowersOfAction = GetFollowersOfDefault(followService: appDelegate.followService)
        let getFollowedByAction = GetFollowedByDefault(followService: appDelegate.followService)
        viewModel = UserInfoViewModel(getTweetsAction: getTweetsAction, createFollowAction: createFollowAction, getFollowersOfAction: getFollowersOfAction, getFollowedByAction: getFollowedByAction)
    }
    
    func setupBindings() {
        bindNickNamesFields()
        bindTables()
        bindButtons()
        // other bindings...
        viewModel.errorPublisher
            .subscribe(onNext: {[weak self] err in self?.onError(error: err) })
            .disposed(by: disposeBag)
        
    }
    
    func bindNickNamesFields() {
        twoWayBind_nickName()
        twoWayBind_nickNameFollowedBy()
        twoWayBind_nickNameFollowing()
    }
    
    func bindTables() {
        bindTweetsTable()
        bindFollowersOfTable()
        bindFollowedByTable()
    }
    
    func twoWayBind_nickName() {
        nickName.rx.text.orEmpty
            .bind(to: viewModel.nickName)
            .disposed(by: disposeBag)
        viewModel.nickName
            .subscribe(onNext: {[weak self] text in self?.nickName.text = text } )
            .disposed(by: disposeBag)
    }
    
    func twoWayBind_nickNameFollowedBy() {
        nickNameFollowedBy.rx.text.orEmpty
            .bind(to: viewModel.nickNameFollowedBy)
            .disposed(by: disposeBag)
        viewModel.nickNameFollowedBy
            .subscribe(onNext: {[weak self] text in self?.nickNameFollowedBy.text = text } )
            .disposed(by: disposeBag)
    }
    
    func twoWayBind_nickNameFollowing() {
        nickNameFollowing.rx.text.orEmpty
            .bind(to: viewModel.nickNameFollowing)
            .disposed(by: disposeBag)
        viewModel.nickNameFollowing
            .subscribe(onNext: {[weak self] text in self?.nickNameFollowing.text = text } )
            .disposed(by: disposeBag)
    }
    
    func bindTweetsTable() {
        viewModel.tweetsPublisher
            .bind(to: tweetsTable
                .rx
                .items(cellIdentifier: tweetsTableCellId, cellType: CustomTableViewCell.self))
                    { row, tweetMsg, cell in
                            cell.textLabel?.text = tweetMsg
                            self.errorMessage.text = ""
                    }
                .disposed(by: disposeBag)
    }
    
    func bindFollowersOfTable() {
        viewModel.followersOfPublisher
            .bind(to: followersOfTable
                .rx
                .items(cellIdentifier: followersOfTableCellId, cellType: CustomTableViewCell.self))
                    { row, tweetMsg, cell in
                            cell.textLabel?.text = tweetMsg
                            self.errorMessage.text = ""
                    }
                .disposed(by: disposeBag)
    }
    
    func bindFollowedByTable() {
        viewModel.followedByPublisher
            .bind(to: followedByTable
                .rx
                .items(cellIdentifier: followedByTableCellId, cellType: CustomTableViewCell.self))
                    { row, tweetMsg, cell in
                            cell.textLabel?.text = tweetMsg
                            self.errorMessage.text = ""
                    }
                .disposed(by: disposeBag)
    }
    
    func bindButtons() {
        viewModel.isGetInfoButtonEnabled
            .bind(to: getInfoButton.rx.isEnabled)
            .disposed(by: disposeBag)

        viewModel.isAddFollowEnabled
            .bind(to: addFollowButton.rx.isEnabled)
            .disposed(by: disposeBag)

        viewModel.isAddFollowerEnabled
            .bind(to: addFollowerButton.rx.isEnabled)
            .disposed(by: disposeBag)
    }
    
    func onError(error: Error) {
        if let e = error as? TwitterKataError {
            errorMessage.text = e.description
        } else {
            errorMessage.text = error.localizedDescription
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        10.0
    }
}
