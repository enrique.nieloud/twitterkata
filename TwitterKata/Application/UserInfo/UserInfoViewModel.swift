//
//  TweetListViewModel.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 30/08/2021.
//

import Foundation
import RxSwift
import RxCocoa

class UserInfoViewModel {
    private let getTweetsAction: GetTweets
    private let createFollowAction: CreateFollow
    private let getFollowersOfAction: GetFollowersOf
    private let getFollowedByAction: GetFollowedBy
    let tweetsPublisher = PublishSubject<[String]>()
    let followersOfPublisher = PublishSubject<[String]>()
    let followedByPublisher = PublishSubject<[String]>()
    let addFollowSucceeded = PublishSubject<Void>()
    let errorPublisher = PublishSubject<Error>()
    let nickName = BehaviorSubject(value: "")
    let nickNameFollowedBy = BehaviorSubject(value: "")
    let nickNameFollowing = BehaviorSubject(value: "")
    let isGetInfoButtonEnabled: Observable<Bool>
    let isAddFollowerEnabled: Observable<Bool>
    let isAddFollowEnabled: Observable<Bool>

    init(getTweetsAction: GetTweets,
         createFollowAction: CreateFollow,
         getFollowersOfAction: GetFollowersOf,
         getFollowedByAction: GetFollowedBy
    ) {
        self.getTweetsAction = getTweetsAction
        self.createFollowAction = createFollowAction
        self.getFollowedByAction = getFollowedByAction
        self.getFollowersOfAction = getFollowersOfAction
        self.isGetInfoButtonEnabled = nickName.asObservable().map({ $0 != "" })
        self.isAddFollowerEnabled = Observable.combineLatest(nickName, nickNameFollowing).map({$0.0 != "" && $0.1 != "" })
        self.isAddFollowEnabled = Observable.combineLatest(nickName, nickNameFollowedBy).map({$0.0 != "" && $0.1 != "" })
        
    }
    
    func getInfo() {
        getTweets()
        getFollowers()
        getFollows()
    }

    func addFollowedByPressed() {
        do {
            try createFollowAction(from: nickNameFollowing.value(), to: nickName.value())
            addFollowSucceeded.onNext(())
        } catch {
            errorPublisher.onNext(error)
        }
        clearFollowAndFollowed()
        getFollowers()
    }

    func addFollowingPressed() {
        do {
            try createFollowAction(from: nickName.value(), to: nickNameFollowedBy.value())
            addFollowSucceeded.onNext(())
        } catch {
            errorPublisher.onNext(error)
        }
        clearFollowAndFollowed()
        getFollows()
    }

    func getTweets() {
        do {
            let tweets = try getTweetsAction(of: nickName.value())
            tweetsPublisher.onNext(tweets.map({$0.message}))
        } catch {
            errorPublisher.onNext(error)
        }
    }
    
    func getFollowers() {
        do {
            let followersOf = try getFollowersOfAction(nickName: nickName.value())
            followersOfPublisher.onNext(followersOf)
        } catch {
            errorPublisher.onNext(error)
        }
        clearFollowAndFollowed()
    }
    
    func getFollows() {
        do {
            let followedBy = try getFollowedByAction(nickName: nickName.value())
            followedByPublisher.onNext(followedBy)
        } catch {
            errorPublisher.onNext(error)
        }
        clearFollowAndFollowed()
    }
    
    func clearFollowAndFollowed() {
        nickNameFollowing.onNext("")
        nickNameFollowedBy.onNext("")
    }
}
