//
//  InitialViewController.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 10/08/2021.
//

import UIKit

class InitialViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func registerUserTouched(_ sender: Any) {
        let vc = RegisterUserViewController(nibName: "RegisterUserViewController", bundle: nil)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func viewUser(_ sender: Any) {
        let vc = UserInfoViewController(nibName: "UserInfoViewController", bundle: nil)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func tweetMessage(_ sender: Any) {
        let vc = CreateTweetViewController(nibName: "CreateTweetViewController", bundle: nil)
        self.present(vc, animated: true, completion: nil)
    }
}
