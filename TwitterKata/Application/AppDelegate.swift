//
//  AppDelegate.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 09/08/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var userService: UserServiceDefault!
    var tweetService: TweetService!
    var followService: FollowService!
    
    func openDB() -> Bool {
        var openDbError: Bool = false
        let dbName = "db.sqlite"
        do {
            let userRepository = try SqliteUserRepository(dbName: dbName)
            userService = UserServiceDefault(userRepository: userRepository)
            let tweetIdGenerator = try SqliteTweetIdGenerator(dbName: dbName)
            let tweetRepository = try SqliteTweetRepository(dbName: dbName)
            tweetService = TweetServiceDefault(tweetIdGenerator: tweetIdGenerator, tweetRepository: tweetRepository, userService: userService)
            let followRepository = try SqliteFollowRepository(dbName: dbName)
            followService = FollowServiceDefault(followRepository: followRepository, userService: userService)
        } catch {
            openDbError = true
        }
        return openDbError
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let _ = openDB()
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

