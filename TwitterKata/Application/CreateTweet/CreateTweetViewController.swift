//
//  CreateTweetViewController.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 30/08/2021.
//

import UIKit
import RxCocoa
import RxSwift

class CreateTweetViewController: UIViewController {
    private var viewModel: CreateTweetViewModel!
    private var disposeBag = DisposeBag()
    @IBOutlet weak var nickName: UITextField!
    @IBOutlet weak var message: UITextView!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var tweetButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorMessage.text = ""
        createViewModel()
        setupBindings()
    }
    
    @IBAction func tweetMessageTouched(_ sender: UIButton) {
        viewModel.tweetMessagePressed()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func createViewModel() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let createTweetAction = CreateTweetDefault(tweetService: appDelegate.tweetService)
        viewModel = CreateTweetViewModel(createTweetAction: createTweetAction)
    }

    func setupBindings() {
        nickName.rx.text.orEmpty
            .bind(to: viewModel.nickName)
            .disposed(by: disposeBag)
        viewModel.nickName
            .subscribe(onNext: {[weak self] text in self?.nickName.text = text } )
            .disposed(by: disposeBag)
        message.rx.text.orEmpty
            .bind(to: viewModel.tweetMessage)
            .disposed(by: disposeBag)
        viewModel.tweetMessage
            .subscribe(onNext: {[weak self] text in self?.message.text = text } )
            .disposed(by: disposeBag)
        viewModel.tweetSucceeded
            .subscribe(onNext: {[weak self] in self?.onTweetSuccess(message: "Message tweeted successfully")})
            .disposed(by: disposeBag)
        viewModel.tweetFailed
            .subscribe(onNext: {[weak self] err in self?.onTweetError(error: err) })
            .disposed(by: disposeBag)
        viewModel.isTweetButtonEnabled
            .bind(to: tweetButton.rx.isEnabled)
            .disposed(by: disposeBag)
    }
    
    func onTweetSuccess(message: String) {
        let alert = UIAlertController(title: "Success", message: message, preferredStyle: .alert )
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil ))
        self.present(alert, animated: true, completion: nil)
    }
    
    func onTweetError(error: Error) {
        var message = "Error tweeting:\n"
        if let e = error as? TwitterKataError {
            message += e.description
        } else {
            message += error.localizedDescription
        }
        self.errorMessage.text = message
        self.errorMessage.isHidden = message == ""
    }
}
