//
//  CreateTweetViewModel.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 30/08/2021.
//

import Foundation
import RxSwift
import RxCocoa


class CreateTweetViewModel {
    private let createTweetAction: CreateTweet
    let tweetSucceeded = PublishSubject<Void>()
    let tweetFailed = PublishSubject<Error>()
    let nickName = BehaviorSubject(value: "")
    let tweetMessage = BehaviorSubject(value: "")
    let isTweetButtonEnabled: Observable<Bool>

    init(createTweetAction: CreateTweet) {
        self.createTweetAction = createTweetAction
        self.isTweetButtonEnabled = Observable.combineLatest(nickName, tweetMessage).map({$0.0 != "" && $0.1 != "" })
    }

    func tweetMessagePressed() {
        do {
            let createTweetData = try CreateTweetData(nickName: nickName.value(), tweetMessage: tweetMessage.value())
            try createTweetAction(createTweetData: createTweetData)
            tweetSucceeded.onNext(())
        } catch {
            tweetFailed.onNext(error)
        }
        nickName.onNext("")
        tweetMessage.onNext("")
    }
}
