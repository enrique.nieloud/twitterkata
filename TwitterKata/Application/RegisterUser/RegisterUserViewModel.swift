//
//  RegisterUserViewModel.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 30/08/2021.
//

import Foundation
import RxSwift
import RxCocoa

class RegisterUserViewModel {
    private let registerUserAction: RegisterUser
    let registerSucceeded = PublishSubject<Void>()
    let registerFailed = PublishSubject<Error>()
    let nickName = BehaviorSubject(value: "")
    let realName = BehaviorSubject(value: "")
    let isRegisterButtonEnabled: Observable<Bool>

    init(registerUserAction: RegisterUser) {
        self.registerUserAction = registerUserAction
        self.isRegisterButtonEnabled = Observable.combineLatest(nickName, realName).map({$0.0 != "" && $0.1 != "" })
    }
    
    func registerButtonPressed() {
        do {
            try registerUserAction(nickName: nickName.value(), realName: realName.value())
            registerSucceeded.onNext(())
        } catch {
            registerFailed.onNext(error)
        }
        nickName.onNext("")
        realName.onNext("")
    }
}
