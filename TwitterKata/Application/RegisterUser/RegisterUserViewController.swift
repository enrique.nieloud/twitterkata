//
//  RegisterUserViewController.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 30/08/2021.
//

import UIKit
import RxCocoa
import RxSwift

class RegisterUserViewController: UIViewController {
    private var viewModel: RegisterUserViewModel!
    private var disposeBag = DisposeBag()
    @IBOutlet weak var nickName: UITextField!
    @IBOutlet weak var realName: UITextField!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var registerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createViewModel()
        setupBindings()
    }

    @IBAction func registerTouched(_ sender: UIButton) {
        viewModel.registerButtonPressed()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func createViewModel() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let registerUserAction = RegisterUserDefault(userService: appDelegate.userService)
        viewModel = RegisterUserViewModel(registerUserAction: registerUserAction)
    }
    
    func setupBindings() {
        nickName.rx.text.orEmpty
            .bind(to: viewModel.nickName)
            .disposed(by: disposeBag)
        viewModel.nickName
            .subscribe(onNext: {[weak self] text in self?.nickName.text = text } )
            .disposed(by: disposeBag)
        realName.rx.text.orEmpty
            .bind(to: viewModel.realName)
            .disposed(by: disposeBag)
        viewModel.realName
            .subscribe(onNext: {[weak self] text in self?.realName.text = text } )
            .disposed(by: disposeBag)
        viewModel.registerSucceeded
            .subscribe(onNext: {[weak self] in self?.onRegisterSuccess(message: "User registered successfully")})
            .disposed(by: disposeBag)
        viewModel.registerFailed
            .subscribe(onNext: {[weak self] err in self?.onRegisterError(error: err) })
            .disposed(by: disposeBag)
        viewModel.isRegisterButtonEnabled
            .bind(to: registerButton.rx.isEnabled)
            .disposed(by: disposeBag)
    }
    
    func onRegisterSuccess(message: String) {
        self.errorMessage.isHidden = true
        let alert = UIAlertController(title: "Success", message: message, preferredStyle: .alert )
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil ))
        self.present(alert, animated: true, completion: nil)
    }
    
    func onRegisterError(error: Error) {
        var message = "Error registering:\n"
        if let e = error as? TwitterKataError {
            message += e.description
        } else {
            message += error.localizedDescription
        }
        self.errorMessage.text = message
        self.errorMessage.isHidden = message == ""
    }
}
