//
//  CustomTableViewCell.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 01/09/2021.
//

import Foundation
import UIKit

class CustomTableViewCell: UITableViewCell {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.textLabel?.font = self.textLabel?.font.withSize(10)
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.textLabel?.font = self.textLabel?.font.withSize(10)
    }
}

