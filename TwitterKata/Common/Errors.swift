//
//  Errors.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 25/08/2021.
//

import Foundation

enum TwitterKataError: Error, Equatable {
    case nickNameNotFound
    case userAlreadyExists
    case toNickNameNotFound
    case fromNickNameNotFound
    case followAlreadyExist
    case userAlreadyExits
    case nickNameMustNotBeEmpty
    case tweetAlreadyExists
    case tweetMessageMustNotBeEmpty
    case sequenceNumberNotFound
    case aUserCannotFollowItself
    var description : String {
        switch self {
        case .nickNameNotFound: return "NickName not found"
        case .userAlreadyExists: return "User already exists"
        case .toNickNameNotFound: return "Followed nickName not found"
        case .fromNickNameNotFound: return "Follower nickName not found"
        case .followAlreadyExist: return "Follow already exist"
        case .userAlreadyExits: return "User already exists"
        case .nickNameMustNotBeEmpty: return "Nick name must not be empty"
        case .tweetAlreadyExists: return "Tweet already exists"
        case .tweetMessageMustNotBeEmpty: return "Tweet message must not be empty"
        case .sequenceNumberNotFound: return "Sequence number not found"
        case .aUserCannotFollowItself: return "A user cannot follow to itself"
      }
    }
}
