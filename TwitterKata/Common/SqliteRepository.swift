//
//  SqliteRepository.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 23/08/2021.
//

import Foundation
import SQLite

class SqliteRepository {
    let db: Connection
    
    init(dbName: String) throws {
        let dbPath = try SqliteRepository.dataBasePath(dbName: dbName)
        db = try Connection(dbPath)
        try setupDB()
    }
    
    static private func dataBasePath(dbName: String) throws -> String {
        #if os(macOS)
            let path = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true).first! + "/TwitterKataREST"
            try FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        #elseif os(iOS)
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        #endif
        return "\(path)/\(dbName)"
    }

    func setupDB() throws {
    }
}

func eraseDBFile(dbName: String) {
    let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    let url = NSURL(fileURLWithPath: path)
    if let pathComponent = url.appendingPathComponent(dbName) {
        let filePath = pathComponent.path
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filePath) {
            do {
                try fileManager.removeItem(atPath: filePath)
            } catch {
            }
        }
    } else {
        print("FILE PATH NOT AVAILABLE")
    }
}
