//
//  Extensions.swift
//  TwitterKata
//
//  Created by Enrique Nieloud on 25/08/2021.
//

import Foundation

// Used for comparing tweets dates
extension Date {
    static func - (lhs: Date, rhs: Date) -> TimeInterval {
        return lhs.timeIntervalSinceReferenceDate - rhs.timeIntervalSinceReferenceDate
    }
}
