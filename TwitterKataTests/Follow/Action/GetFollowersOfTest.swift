//
//  GetFollowersOfTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 17/08/2021.
//

import XCTest
import SwiftyMocky

@testable import TwitterKata

class GetFollowersOfTest: XCTestCase {
    var getFollowersOf: GetFollowersOf! // Object under Test
    var followServiceMock: FollowServiceMock! 
    var followers: [String] = []

    func givenUserServiceCreatedWithBobAndQuique_QuiqueFollowsBob() {
        followServiceMock = FollowServiceMock()
        Given(followServiceMock, .followers(of: .value("Bob"), willReturn: ["Quique"]) )
        getFollowersOf = GetFollowersOfDefault(followService: followServiceMock)
    }
    
    func whenGetFollowersOfIsExecuted() throws {
        try followers = getFollowersOf(nickName: "Bob")
    }
    
    func thenFollowService_Followers_MustHaveBeenCalled() throws {
        Verify(followServiceMock, .once, .followers(of: .any))
    }
    
    func testGetFollowersOf() throws {
        givenUserServiceCreatedWithBobAndQuique_QuiqueFollowsBob()
        try whenGetFollowersOfIsExecuted()
        try thenFollowService_Followers_MustHaveBeenCalled()
    }
}
