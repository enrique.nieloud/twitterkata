//
//  CreateFollowTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 12/08/2021.
//

import XCTest
import SwiftyMocky

@testable import TwitterKata

class CreateFollowTest: XCTestCase {
    var createFollow: CreateFollow! // Object under Test
    var followServiceMock: FollowServiceMock!

    func givenFollowService(/*whenReceives nickName: String, thenFollowersOfWillReturn nickNameToReturn: String*/) {
        followServiceMock = FollowServiceMock()
        createFollow = CreateFollowDefault(followService: followServiceMock)
    }
    
    func whenCreateFollow(from fromNickName: String, to toNickName: String) throws {
        try createFollow(from: fromNickName, to: toNickName)
    }
    
    func thenFollowServiceAddMustHaveBeenCalledWith(from fromNickName: String, to toNickName: String) throws {
        Verify(followServiceMock, .add(follow: .value(Follow(fromNickName: fromNickName, toNickName: toNickName))))
    }
    
    func testCreateFollow() throws {
        givenFollowService()
        try whenCreateFollow(from: "Quique", to: "Bob")
        try thenFollowServiceAddMustHaveBeenCalledWith(from: "Quique", to: "Bob")
    }
}
