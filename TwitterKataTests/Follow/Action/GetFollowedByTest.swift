//
//  GetFollowedByTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 17/08/2021.
//

import XCTest
import SwiftyMocky

@testable import TwitterKata

class GetFollowedByTest: XCTestCase {
    var getFollowedBy: GetFollowedBy! // Object under Test
    var followServiceMock: FollowServiceMock! 
    var followed: [String] = []

    func givenUserServiceCreatedWithBobAndQuique_QuiqueFollowsBob() {
        followServiceMock = FollowServiceMock()
        Given(followServiceMock, .followed(by: .value("Quique"), willReturn: ["Bob"]))
        getFollowedBy = GetFollowedByDefault(followService: followServiceMock)
    }
    
    func whenGetFollowedByIsExecuted() throws {
        try followed = getFollowedBy(nickName: "Quique")
    }
    
    func thenFollowService_Followed_MustHaveBeenCalled() throws {
        Verify(followServiceMock, .once, .followed(by: .any))
    }
    
    func testGetFollowersOf() throws {
        givenUserServiceCreatedWithBobAndQuique_QuiqueFollowsBob()
        try whenGetFollowedByIsExecuted()
        try thenFollowService_Followed_MustHaveBeenCalled()
    }
}
