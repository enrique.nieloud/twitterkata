//
//  FollowServiceTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 17/08/2021.
//

import XCTest
import SwiftyMocky

@testable import TwitterKata

class FollowService_AddFollowTest: XCTestCase {
    var followService: FollowService! // Object under Test
    var followRepositoryMock: FollowRepositoryMock!
    var userServiceMock: UserServiceMock!

    func given_followService_with_users_bob_and_quique() {
        followRepositoryMock = FollowRepositoryMock()
        userServiceMock = UserServiceMock()
        Given(userServiceMock, .exists(nickName: .value("Quique"), willReturn: true))
        Given(userServiceMock, .exists(nickName: .value("Bob"), willReturn: true))
        followService = FollowServiceDefault(followRepository: followRepositoryMock, userService: userServiceMock)
    }
    
    func when_follow_from_quique_to_bob_is_added() throws {
        try followService.add(follow: Follow(fromNickName: "Quique", toNickName: "Bob"))
    }
    
    func then_function_exists_from_user_service_must_have_been_called_with_bob_and_quique() {
        Verify(userServiceMock, .once, .exists(nickName: .value("Bob")))
        Verify(userServiceMock, .once, .exists(nickName: .value("Quique")))
    }
    
    func then_follow_repository_save_must_have_been_called_once() {
        Verify(followRepositoryMock, .once, .save(follow: .value(Follow(fromNickName: "Quique", toNickName: "Bob"))))
    }
    
    func test_add_follow() throws {
        given_followService_with_users_bob_and_quique()
        try when_follow_from_quique_to_bob_is_added()
        then_function_exists_from_user_service_must_have_been_called_with_bob_and_quique()
        then_follow_repository_save_must_have_been_called_once()
    }
}

class FollowService_AddFollowToItselfTest: XCTestCase {
    var followService: FollowService! // Object under Test
    var followRepositoryMock: FollowRepositoryMock!
    var userServiceMock: UserServiceMock!
    var twitterKataError: TwitterKataError!

    func given_followService() {
        followRepositoryMock = FollowRepositoryMock()
        userServiceMock = UserServiceMock()
        Given(userServiceMock, .exists(nickName: .value("Quique"), willReturn: true))
        followService = FollowServiceDefault(followRepository: followRepositoryMock, userService: userServiceMock)
    }
    
    func when_follow_from_quique_to_quique_is_added() throws {
        do {
        try followService.add(follow: Follow(fromNickName: "Quique", toNickName: "Quique"))
        } catch let tkError as TwitterKataError {
            twitterKataError = tkError
        }
    }
    
    func then_exception_thown_must_be_aUserCannotFollowItself() {
        XCTAssert(twitterKataError == .aUserCannotFollowItself)
    }
    
    func test_add_follow() throws {
        given_followService()
        try when_follow_from_quique_to_quique_is_added()
        then_exception_thown_must_be_aUserCannotFollowItself()
    }
}

class FollowService_Followers: XCTestCase {
    var followService: FollowService! // Object under Test
    var followRepositoryMock: FollowRepositoryMock!
    var userServiceMock: UserServiceMock!

    func given_followService() {
        followRepositoryMock = FollowRepositoryMock()
        userServiceMock = UserServiceMock()
        Given(followRepositoryMock, .findFollowers(of: .value("Bob"), willReturn: ["Quique"]))
        followService = FollowServiceDefault(followRepository: followRepositoryMock, userService: userServiceMock)
    }
    
    func when_followers_function_is_called() throws {
        _ = try followService.followers(of: "Bob")
    }
    
    func then_function_findFollowers_from_followRepository_must_have_been_called_once() {
        Verify(followRepositoryMock, .once, .findFollowers(of: .any))
    }
    
    func test_add_follow() throws {
        given_followService()
        try when_followers_function_is_called()
        then_function_findFollowers_from_followRepository_must_have_been_called_once()
    }
}

class FollowService_Followed: XCTestCase {
    var followService: FollowService! // Object under Test
    var followRepositoryMock: FollowRepositoryMock!
    var userServiceMock: UserServiceMock!

    func given_followService() {
        followRepositoryMock = FollowRepositoryMock()
        userServiceMock = UserServiceMock()
        Given(followRepositoryMock, .findFollowed(by: .value("Quique"), willReturn: ["Bob"]))
        followService = FollowServiceDefault(followRepository: followRepositoryMock, userService: userServiceMock)
    }
    
    func when_followed_function_is_called() throws {
        _ = try followService.followed(by: "Quique")
    }
    
    func then_function_findFollowed_from_followRepository_must_have_been_called_once() {
        Verify(followRepositoryMock, .once, .findFollowed(by: .any))
    }
    
    func test_add_follow() throws {
        given_followService()
        try when_followed_function_is_called()
        then_function_findFollowed_from_followRepository_must_have_been_called_once()
    }
}
