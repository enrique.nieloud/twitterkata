//
//  NonPersistentFollowRepositoryTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 17/08/2021.
//

import Foundation


import XCTest
import SwiftyMocky

@testable import TwitterKata

class InMemoryFollowRepositoryTest: XCTestCase {
    var followRepository: InMemoryFollowRepository! // Object under Test

    func givenEmptyFollowRepository() {
        followRepository = InMemoryFollowRepository()
    }
    
    func whenAddQuiqueFollowsBob() throws {
        try followRepository.save(follow: Follow(fromNickName: "Quique", toNickName: "Bob"))
        try followRepository.save(follow: Follow(fromNickName: "Quique", toNickName: "Mark"))
        try followRepository.save(follow: Follow(fromNickName: "Landau", toNickName: "Bob"))
    }
    
    func thenFollowerQuiqueMustExists() throws {
        XCTAssert(try followRepository.findFollowers(of: "Bob") == ["Quique", "Landau"])
    }
    
    func thenFollowedBobMustExists() throws {
        XCTAssert(try followRepository.findFollowed(by: "Quique") == ["Bob","Mark"])
    }
    
    func thenFollowersMustBeEmpty() throws {
        XCTAssert(try followRepository.findFollowers(of: "Quique") == [])
    }
    
    func thenFollowedMustBeEmpty() throws {
        XCTAssert(try followRepository.findFollowed(by: "Quique") == [])
    }

    func testFollowersExistsAfterAddFollow() throws {
        givenEmptyFollowRepository()
        try whenAddQuiqueFollowsBob()
        try thenFollowerQuiqueMustExists()
    }
    
    func testFollowedExistsAfterAddFollow() throws {
        givenEmptyFollowRepository()
        try whenAddQuiqueFollowsBob()
        try thenFollowedBobMustExists()
    }
    
    func testEmptyRepositoryHasNoFollowers() throws {
        givenEmptyFollowRepository()
        try thenFollowersMustBeEmpty()
    }
    
    func testEmptyRepositoryHasNoFollowed() throws {
        givenEmptyFollowRepository()
        try thenFollowedMustBeEmpty()
    }
}
