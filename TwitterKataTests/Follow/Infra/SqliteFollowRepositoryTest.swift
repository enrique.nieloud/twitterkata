//
//  SqliteFollowRepositoryTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 23/08/2021.
//

import XCTest
import SwiftyMocky

@testable import TwitterKata

class SqliteFollowRepositoryTest: XCTestCase {
    var followRepository: SqliteFollowRepository! // Object under Test

    func givenEmptyFollowRepository() throws {
        let dbName = "db.sqlite"
        eraseDBFile(dbName: dbName)
        followRepository = try SqliteFollowRepository(dbName: dbName)
    }
    
    func whenAddQuiqueFollowsBob() throws {
        try followRepository.save(follow: Follow(fromNickName: "Quique", toNickName: "Bob"))
        try followRepository.save(follow: Follow(fromNickName: "Quique", toNickName: "Mark"))
        try followRepository.save(follow: Follow(fromNickName: "Landau", toNickName: "Bob"))
    }
    
    func thenFollowerQuiqueMustExists() throws {
        XCTAssert(try followRepository.findFollowers(of: "Bob") == ["Quique", "Landau"])
    }
    
    func thenFollowedBobMustExists() throws {
        XCTAssert(try followRepository.findFollowed(by: "Quique") == ["Bob","Mark"])
    }
    
    func thenFollowersMustBeEmpty() throws {
        XCTAssert(try followRepository.findFollowers(of: "Quique") == [])
    }
    
    func thenFollowedMustBeEmpty() throws {
        XCTAssert(try followRepository.findFollowed(by: "Quique") == [])
    }
    
    func testFollowersExistsAfterAddFollow() throws {
        try givenEmptyFollowRepository()
        try whenAddQuiqueFollowsBob()
        try thenFollowerQuiqueMustExists()
    }
    
    func testFollowedExistsAfterAddFollow() throws {
        try givenEmptyFollowRepository()
        try whenAddQuiqueFollowsBob()
        try thenFollowedBobMustExists()
    }
    
    func testEmptyRepositoryHasNoFollowers() throws {
        try givenEmptyFollowRepository()
        try thenFollowersMustBeEmpty()
    }
    
    func testEmptyRepositoryHasNoFollowed() throws {
        try givenEmptyFollowRepository()
        try thenFollowedMustBeEmpty()
    }
}
