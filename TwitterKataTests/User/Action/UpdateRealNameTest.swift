//
//  UpdateRealNameTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 11/08/2021.
//

import XCTest
import SwiftyMocky

@testable import TwitterKata

class UpdateRealNameTest: XCTestCase {
    var updateRealName: UpdateRealName! // Object under Test
    var userServiceMock: UserServiceMock!
    
    func given_UpdateRealName() throws {
        userServiceMock = UserServiceMock() 
        updateRealName = UpdateRealNameDefault(userService: userServiceMock)
    }
    
    func when_user_with(nickName: String, updatesRealNameTo realName: String) throws {
        try updateRealName(nickName: nickName, updatedRealName: realName)
    }
    
    func then_userService_must_have_called_updateRealName_with(nickName: String, andRealNameChangedTo realName: String) throws {
        Verify(userServiceMock, .once, .updateRealName(nickName: .value(nickName), updatedRealName: .value(realName)))
    }
    
    func testUpdateUser() throws {
        try given_UpdateRealName()
        try when_user_with(nickName: "Bob", updatesRealNameTo: "Roberto")
        try then_userService_must_have_called_updateRealName_with(nickName: "Bob", andRealNameChangedTo: "Roberto")
    }
}
