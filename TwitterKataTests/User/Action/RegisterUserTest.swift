//
//  TwitterKataTests.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 09/08/2021.
//

import XCTest
import SwiftyMocky

@testable import TwitterKata

class RegisterUserTest: XCTestCase {
    var registerUser: RegisterUser! // Object under Test
    var userServiceMock: UserServiceMock!
    
    func givenUserService() {
        userServiceMock = UserServiceMock()
        registerUser = RegisterUserDefault(userService: userServiceMock)
    }
    
    func whenRegisterUser(nickName: String, realName: String) throws {
        try registerUser(nickName: nickName, realName: realName)
    }
    
    func then_userService_add_Must_have_been_called(nickName: String, realName: String) {
        Verify(userServiceMock, .once, .add(user: .value(User(nickName: nickName, realName: realName))))
    }
    
    func testCreateUser() throws {
        givenUserService()
        try whenRegisterUser(nickName: "Bob", realName: "Robert")
        then_userService_add_Must_have_been_called(nickName: "Bob", realName: "Robert")
    }
}
