//
//  SqliteUserRepositoryTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 22/08/2021.
//

import Foundation

import XCTest
@testable import TwitterKata

class SqliteUserRepositoryTest: XCTestCase {
    
    var repository: SqliteUserRepository! // Object under Test

    func givenARepository() throws {
        let dbName = "db.sqlite"
        eraseDBFile(dbName: dbName)
        repository = try SqliteUserRepository(dbName: dbName)
    }

    func whenRegisterUser(user: User) throws {
        try repository.save(user: user)
    }

    func thenUserMustExists(user: User) {
        XCTAssert(repository.exists(nickName: user.nickName))
    }

    func testCreateUser() throws {
        let user = User(nickName: "Bob", realName: "Robert")
        try givenARepository()
        try whenRegisterUser(user: user)
        thenUserMustExists(user: user)
    }
    
    func thenFindUserMustBeEqualToUser(user: User) throws {
        let userFound = try repository.findBy(nickName: user.nickName)
        XCTAssert(userFound.nickName == user.nickName)
    }

    func testFindUser() throws {
        let user = User(nickName: "Bob", realName: "Robert")
        try givenARepository()
        try whenRegisterUser(user: user)
        try thenFindUserMustBeEqualToUser(user: user)
    }
    
    func givenARepositoryCreatedWith(user: User) throws {
        let dbName = "db.sqlite"
        eraseDBFile(dbName: dbName)
        repository = try SqliteUserRepository(dbName: dbName)
        let user = User(nickName: "Bob", realName: "Robert")
        try repository.save(user: user)
    }

    func whenUserWith(nickName: String, updatesRealNameTo realName: String) throws {
        try repository.update(user: User(nickName: "Bob", realName: "Roberto"))
    }

    func thenUserWith(nickName: String, mustHaveItsNameChangedTo realName: String) throws {
        let userFound = try repository.findBy(nickName: nickName)
        XCTAssert(userFound.realName == realName)
    }

    func testUpdateUser() throws {
        try givenARepositoryCreatedWith(user: User(nickName: "Bob", realName: "Robert"))
        try whenUserWith(nickName: "Bob", updatesRealNameTo: "Roberto")
        try thenUserWith(nickName: "Bob", mustHaveItsNameChangedTo: "Roberto")
    }
    
}
