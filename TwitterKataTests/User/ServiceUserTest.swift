//
//  ServiceUserTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 10/08/2021.
//

import XCTest
import SwiftyMocky

@testable import TwitterKata

class ServiceUserRegisterTest: ServiceUserTestBase {
    func whenRegisterUser(nickName: String, realName: String) {
        registerUser(nickName: nickName, realName: realName)
    }

    func then_user_repository_must_have_called_save_with(nickName: String, andRealName realName: String) {
        let user = User(nickName: nickName, realName: realName)
        Verify(userRepositoryMock, .once, .save(user: .value(user)))
    }
    
    func testRegisterUser() {
        let nickName = "Bob"
        let realName = "Robert"
        givenModelCreated()
        Given(userRepositoryMock, .exists(nickName: .any, willReturn: false))
        whenRegisterUser(nickName: nickName, realName: realName)
        then_user_repository_must_have_called_save_with(nickName: nickName, andRealName: realName)
    }
}

class ServiceUserRegisterEmptyNickNameTest: ServiceUserTestBase {
    func whenRegisterUser(nickName: String, realName: String) {
        registerUser(nickName: nickName, realName: realName)
    }

    func thenUserMustExists(nickName: String, realName: String) {
        let user = User(nickName: nickName, realName: realName)
        XCTAssert(userService.exists(nickName: user.nickName))
    }
    
    func testRegisterUserEmptyNickName() {
        let nickName = ""
        let realName = "Robert"
        givenModelCreated()
        whenRegisterUser(nickName: nickName, realName: realName)
        thenErrorThrownMustBe(expectedError: .nickNameMustNotBeEmpty)
    }
}

class ServiceUserDuplicateUserTest: ServiceUserTestBase {
    func whenRegisterDuplicatedUser(nickName: String, realName: String) {
        Given(userRepositoryMock, .exists(nickName: .value("Quique"), willReturn: false))
        registerUser(nickName: nickName, realName: realName) // register it for first time...
        if errorThrown != nil {
            return
        }
        Given(userRepositoryMock, .exists(nickName: .value("Quique"), willReturn: true))
        registerUser(nickName: nickName, realName: realName) // and again!
    }
    
    func testDuplicateUser() {
        givenModelCreated()
        whenRegisterDuplicatedUser(nickName: "Quique", realName: "Enrique")
        thenErrorThrownMustBe(expectedError: .userAlreadyExits)
    }
}


class ServiceUserUpdateTest: ServiceUserTestBase {

    func givenAModelCreatedWith(user: User) throws {
        givenModelCreated()
        Given(userRepositoryMock, .exists(nickName: .value(user.nickName), willReturn: false))
        try userService.add(user: user)
    }

    func whenUserWith(nickName: String, updatesRealNameTo realName: String) throws {
        try userService.updateRealName(nickName: nickName, updatedRealName: realName)
    }

    func thenUserWith(nickName: String, mustHaveItsNameChangedTo realName: String) throws {
        Given(userRepositoryMock, .findBy(nickName: .value(nickName), willReturn: User(nickName: nickName, realName: realName)))
        let userFound = try userService.findBy(nickName: nickName)
        XCTAssert(userFound.realName == realName)
    }

    func testUpdateUser() throws {
        try givenAModelCreatedWith(user: User(nickName: "Bob", realName: "Robert"))
        try whenUserWith(nickName: "Bob", updatesRealNameTo: "Roberto")
        try thenUserWith(nickName: "Bob", mustHaveItsNameChangedTo: "Roberto")
    }
}

class ServiceUserFindInexistentUserTest: ServiceUserTestBase {

    func givenAModelCreatedWith(user: User) throws {
        givenModelCreated()
        Given(userRepositoryMock, .exists(nickName: .value(user.nickName), willReturn: false))
        try userService.add(user: user)
    }

    func whenFindingUserWith(nickName: String) {
        errorThrown = nil
        Given(userRepositoryMock, .findBy(nickName: .value(nickName), willThrow: TwitterKataError.nickNameNotFound))
        do {
            try _ = userService.findBy(nickName: nickName)
        } catch let error {
            // if an error is thrown, save it in errorThrown
            errorThrown = error
        }
    }

    func testUpdateUser() throws {
        try givenAModelCreatedWith(user: User(nickName: "Bob", realName: "Robert"))
        whenFindingUserWith(nickName: "Boby")
        thenErrorThrownMustBe(expectedError: .nickNameNotFound)
    }
}
