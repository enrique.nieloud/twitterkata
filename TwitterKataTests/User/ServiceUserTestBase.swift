//
//  ServiceUserTestBase.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 10/08/2021.
//

import XCTest
@testable import TwitterKata

class ServiceUserTestBase: XCTestCase {
    var userService: UserServiceDefault!         // object to be tested
    var userRepositoryMock: UserRepositoryMock!
    var errorThrown: Error?
    
    func createModel() {
        userRepositoryMock = UserRepositoryMock()
        userService = UserServiceDefault(userRepository: userRepositoryMock)
    }

    func registerUser(nickName: String, realName: String) {
        errorThrown = nil
        let newUser = User(nickName: nickName, realName: realName)
        do {
            try userService.add(user: newUser)
        } catch let error {
            // if an error is thrown, save it in errorThrown
            errorThrown = error
        }
    }
    
    func givenModelCreated() {
        createModel()
    }
    
    func thenErrorThrownMustBe(expectedError: TwitterKataError) {
        if let error = errorThrown as? TwitterKataError {
            XCTAssert(error == expectedError)
        } else {
            XCTAssert(false,  "\(expectedError.description) Exception was expected")
        }
    }
}
