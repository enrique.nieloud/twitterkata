//
//  SqliteTweetRepositoryTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 25/08/2021.
//

import XCTest
import SwiftyMocky

@testable import TwitterKata



class SqliteTweetRepositoryTest: XCTestCase {
    var repository: SqliteTweetRepository!
    
    func givenRepositoryCreated() throws {
        let dbName = "db.sqlite"
        eraseDBFile(dbName: dbName)
        repository = try SqliteTweetRepository(dbName: dbName)
    }
}

class SqliteTweetRepositoryTest_AddNewTweet: SqliteTweetRepositoryTest {
    var tweet = Tweet(id: 1, date: Date(), nickName: "Quique", message: "Hello")
    
    func when_new_tweet_is_added() throws {
        try repository.save(tweet: tweet)
    }
    
    func then_new_tweet_must_exist() throws {
        let tweetsFound = try repository.findTweets(of: tweet.nickName)
        XCTAssert(tweetsFound == [tweet])
    }
    
    func test() throws {
        try givenRepositoryCreated()
        try when_new_tweet_is_added()
        try then_new_tweet_must_exist()
    }
}

class SqliteTweetRepositoryTest_FindInexistentTweet: SqliteTweetRepositoryTest {
    
    func then_find_tweets_must_by_empty() throws {
        XCTAssert(try repository.findTweets(of: "Quique") == [])
    }
    
    func test() throws {
        try givenRepositoryCreated()
        try then_find_tweets_must_by_empty()
    }
}
