//
//  SqliteTweetIdGeneratorTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 25/08/2021.
//

import XCTest
import SwiftyMocky

@testable import TwitterKata

class SqliteTweetIdGeneratorTest: XCTestCase {
    
    var tweetIdGenerator: SqliteTweetIdGenerator!
    var TweetID1: TweetID = 0
    var TweetID2: TweetID = 0
    
    func givenTweetIdGeneratorInstanciated() throws {
        let dbName = "db.sqlite"
        eraseDBFile(dbName: dbName)
        tweetIdGenerator = try SqliteTweetIdGenerator(dbName: dbName)
    }
    
    func whenIncrementIsCalledTwice() throws {
        TweetID1 = try tweetIdGenerator()
        TweetID2 = try tweetIdGenerator()
    }
    
    func Then_TweetId2_ShouldBeGreaterThan_TweetId1() throws {
        XCTAssert(TweetID2 > TweetID1)
    }

    func test() throws {
        try givenTweetIdGeneratorInstanciated()
        try whenIncrementIsCalledTwice()
        try Then_TweetId2_ShouldBeGreaterThan_TweetId1()
    }
}
