//
//  InMemoryTweetRepositoryTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 25/08/2021.
//

import XCTest
import SwiftyMocky

@testable import TwitterKata

class InMemoryTweetRepositoryTest_AddNewTweet: XCTestCase {
    var repository = InMemoryTweetRepository()
    var tweet = Tweet(id: 1, date: Date(), nickName: "Quique", message: "Hello")
    
    func givenRepositoryCreated() {
        // the repo is already created
    }
    
    func when_new_tweet_is_added() throws {
        try repository.save(tweet: tweet)
    }
    
    func then_new_tweet_must_exist() throws {
        XCTAssert(try repository.findTweets(of: tweet.nickName) == [tweet])
    }
    
    func test() throws {
        givenRepositoryCreated()
        try when_new_tweet_is_added()
        try then_new_tweet_must_exist()
    }
}

class InMemoryTweetRepositoryTest_FindInexistentTweet: XCTestCase {
    var repository = InMemoryTweetRepository()
    
    func givenRepositoryCreated() {
        // the repo is already created
    }
    
    func then_find_tweets_must_by_empty() throws {
        XCTAssert(try repository.findTweets(of: "Quique") == [])
    }
    
    func test() throws {
        givenRepositoryCreated()
        try then_find_tweets_must_by_empty()
    }
}
