//
//  InMemoryIdGeneratorTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 25/08/2021.
//

import XCTest
import SwiftyMocky

@testable import TwitterKata

class InMemoryIdGeneratorTest: XCTestCase {
    
    var tweetIdGenerator = InMemoryTweetIdGenerator()
    var TweetID1: TweetID = 0
    var TweetID2: TweetID = 0
    
    func givenTweetIdGeneratorInstanciated() {
    }
    
    func whenIncrementIsCalledTwice() throws {
        TweetID1 = try tweetIdGenerator()
        TweetID2 = try tweetIdGenerator()
    }
    
    func Then_TweetId2_ShouldBeGreaterThan_TweetId1() throws {
        XCTAssert(TweetID2 > TweetID1)
    }

    func test() throws {
        givenTweetIdGeneratorInstanciated()
        try whenIncrementIsCalledTwice()
        try Then_TweetId2_ShouldBeGreaterThan_TweetId1()
    }
}
