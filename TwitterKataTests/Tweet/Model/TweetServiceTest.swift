//
//  TweetServiceTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 25/08/2021.
//

import XCTest
import SwiftyMocky

@testable import TwitterKata

class TweetService_CreateTweet_Test: XCTestCase {
    var tweetService: TweetService! // Object under Test
    var tweetIdGeneratorMock: TweetIdGeneratorMock!
    var tweetRepositoryMock: TweetRepositoryMock!
    var userServiceMock: UserServiceMock!

    func givenTweetServiceInstanciated() {
        tweetIdGeneratorMock = TweetIdGeneratorMock()
        tweetRepositoryMock = TweetRepositoryMock()
        userServiceMock = UserServiceMock()
        tweetService = TweetServiceDefault(tweetIdGenerator: tweetIdGeneratorMock, tweetRepository: tweetRepositoryMock, userService: userServiceMock)
    }
    
    func given_UserService_will_return_true_on_exist() {
        Given(userServiceMock, .exists(nickName: .any, willReturn: true))
    }

    func given_IdGenerator_will_return_number_1() {
        Given(tweetIdGeneratorMock, .callAsFunction(willReturn: 1))
    }

    func whenCreateTweetCalled(withNickName nickName: String, AndTweetMessage message: String) throws {
        let createTweetData = CreateTweetData(nickName: nickName, tweetMessage: message)
        try tweetService.createTweet(createTweetData: createTweetData)
    }
    
    func then_save_in_repository_Must_have_been_called_once() {
        Verify(tweetRepositoryMock, .once, .save(tweet: .any))
    }
    
    func then_id_generator_must_have_been_called_once() {
        Verify(tweetIdGeneratorMock, .once, .callAsFunction())
    }
    
    func then_userService_exists_must_have_been_called_with(nickName: String) {
        Verify(userServiceMock, .atLeastOnce, .exists(nickName: .value(nickName)))
    }
    
    func testCreateTweet() throws {
        // Given
        givenTweetServiceInstanciated()
        given_UserService_will_return_true_on_exist()
        given_IdGenerator_will_return_number_1()

        // When
        try whenCreateTweetCalled(withNickName: "Quique", AndTweetMessage: "Hello")

        // Then
        then_id_generator_must_have_been_called_once()
        then_userService_exists_must_have_been_called_with(nickName: "Quique")
        then_save_in_repository_Must_have_been_called_once()
    }

}
class TweetService_GetTweets_Test: XCTestCase {
    var tweetService: TweetService! // Object under Test
    var tweetIdGeneratorMock: TweetIdGeneratorMock!
    var tweetRepositoryMock: TweetRepositoryMock!
    var userServiceMock: UserServiceMock!

    func givenTweetServiceInstanciated() {
        tweetIdGeneratorMock = TweetIdGeneratorMock()
        tweetRepositoryMock = TweetRepositoryMock()
        userServiceMock = UserServiceMock()
        tweetService = TweetServiceDefault(tweetIdGenerator: tweetIdGeneratorMock, tweetRepository: tweetRepositoryMock, userService: userServiceMock)
    }
    
    func given_that_tweetRepository_will_return_some_teets_array() {
        Given(tweetRepositoryMock, .findTweets(of: .any, willReturn: [Tweet]() ))
    }
    
    func given_UserService_will_return_true_on_exist() {
        Given(userServiceMock, .exists(nickName: .any, willReturn: true))
    }

    func whenGetTweets(of nickName: String) throws {
        let _ = try tweetService.getTweets(of: nickName)
    }
    
    func then_tweetRepositoryMock_findTweets_will_be_called_once(nickName: String) {
        Verify(tweetRepositoryMock, .once, .findTweets(of: .value(nickName)))
    }
    
    func testGetTweets() throws {
        givenTweetServiceInstanciated()
        given_that_tweetRepository_will_return_some_teets_array()
        given_UserService_will_return_true_on_exist()
        try whenGetTweets(of: "Quique")
        then_tweetRepositoryMock_findTweets_will_be_called_once(nickName: "Quique")
    }
}

