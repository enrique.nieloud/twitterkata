//
//  CreateTweetTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 24/08/2021.
//

import XCTest
import SwiftyMocky

@testable import TwitterKata

class CreateTweetTest: XCTestCase {
    var createTweet: CreateTweet! // Object under Test
    var tweetServiceMock: TweetServiceMock!
    
    func givenCreateTweetActionInstanciated() {
        tweetServiceMock = TweetServiceMock()
        createTweet = CreateTweetDefault(tweetService: tweetServiceMock)
    }
    
    func whenCreateTweet(from fromNickName: String, withMessage message: String) throws {
        let createTweetData = CreateTweetData(nickName: fromNickName, tweetMessage: message)
        try createTweet(createTweetData: createTweetData)
    }
    
    func thenTweetServiceMustHaveBeenCalled(with nickName: String, andMessage message: String) throws {
        let createTweetData = CreateTweetData(nickName: nickName, tweetMessage: message)
        Verify(tweetServiceMock, 1, .createTweet(createTweetData: .value(createTweetData)))
    }
    
    func testCreateTweet() throws {
        givenCreateTweetActionInstanciated()
        try whenCreateTweet(from: "Quique", withMessage: "Hello")
        try thenTweetServiceMustHaveBeenCalled(with: "Quique", andMessage: "Hello")
    }
}
