//
//  GetTweetsTest.swift
//  TwitterKataTests
//
//  Created by Enrique Nieloud on 24/08/2021.
//

import XCTest
import SwiftyMocky

@testable import TwitterKata

class GetTweetsTest: XCTestCase {
    var getTweets: GetTweets! // Object under Test
    var tweetServiceMock: TweetServiceMock!
    let serviceTweets = [Tweet(id: 1, date: Date(), nickName: "Quique", message: "Hello")]
    var returnedTweets : [Tweet] = []

    func givenGetTweetsActionInstanciated() {
        tweetServiceMock = TweetServiceMock()
        getTweets = GetTweetsDefault(tweetService: tweetServiceMock)
    }
    
    func givenTweetServiceWithOneTweetFrom(from fromNickName: String, withMessage message: String) {
        Given(tweetServiceMock, .getTweets(of: .value(fromNickName), willReturn: serviceTweets))
    }
    
    func whenGetTweetsOfIsCalled(from fromNickName: String) throws {
        returnedTweets = try getTweets(of: fromNickName)
    }
    
    func thenTweetsMustBe(expectedTweets: [Tweet]) {
        XCTAssert(returnedTweets == expectedTweets)
    }
    
    func testCreateTweet() throws {
        givenGetTweetsActionInstanciated()
        givenTweetServiceWithOneTweetFrom(from: "Quique", withMessage: "Hello")
        try whenGetTweetsOfIsCalled(from: "Quique")
        thenTweetsMustBe(expectedTweets: serviceTweets)
    }
}
