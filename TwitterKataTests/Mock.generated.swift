// Generated using Sourcery 1.0.2 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT



// Generated with SwiftyMocky 4.0.1

import SwiftyMocky
import XCTest
import Foundation
@testable import TwitterKata


// MARK: - FollowRepository

open class FollowRepositoryMock: FollowRepository, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func save(follow: Follow) throws {
        addInvocation(.m_save__follow_follow(Parameter<Follow>.value(`follow`)))
		let perform = methodPerformValue(.m_save__follow_follow(Parameter<Follow>.value(`follow`))) as? (Follow) -> Void
		perform?(`follow`)
		do {
		    _ = try methodReturnValue(.m_save__follow_follow(Parameter<Follow>.value(`follow`))).casted() as Void
		} catch MockError.notStubed {
			// do nothing
		} catch {
		    throw error
		}
    }

    open func findFollowers(of nickName: String) throws -> [String] {
        addInvocation(.m_findFollowers__of_nickName(Parameter<String>.value(`nickName`)))
		let perform = methodPerformValue(.m_findFollowers__of_nickName(Parameter<String>.value(`nickName`))) as? (String) -> Void
		perform?(`nickName`)
		var __value: [String]
		do {
		    __value = try methodReturnValue(.m_findFollowers__of_nickName(Parameter<String>.value(`nickName`))).casted()
		} catch MockError.notStubed {
			onFatalFailure("Stub return value not specified for findFollowers(of nickName: String). Use given")
			Failure("Stub return value not specified for findFollowers(of nickName: String). Use given")
		} catch {
		    throw error
		}
		return __value
    }

    open func findFollowed(by nickName: String) throws -> [String] {
        addInvocation(.m_findFollowed__by_nickName(Parameter<String>.value(`nickName`)))
		let perform = methodPerformValue(.m_findFollowed__by_nickName(Parameter<String>.value(`nickName`))) as? (String) -> Void
		perform?(`nickName`)
		var __value: [String]
		do {
		    __value = try methodReturnValue(.m_findFollowed__by_nickName(Parameter<String>.value(`nickName`))).casted()
		} catch MockError.notStubed {
			onFatalFailure("Stub return value not specified for findFollowed(by nickName: String). Use given")
			Failure("Stub return value not specified for findFollowed(by nickName: String). Use given")
		} catch {
		    throw error
		}
		return __value
    }


    fileprivate enum MethodType {
        case m_save__follow_follow(Parameter<Follow>)
        case m_findFollowers__of_nickName(Parameter<String>)
        case m_findFollowed__by_nickName(Parameter<String>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_save__follow_follow(let lhsFollow), .m_save__follow_follow(let rhsFollow)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsFollow, rhs: rhsFollow, with: matcher), lhsFollow, rhsFollow, "follow"))
				return Matcher.ComparisonResult(results)

            case (.m_findFollowers__of_nickName(let lhsNickname), .m_findFollowers__of_nickName(let rhsNickname)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsNickname, rhs: rhsNickname, with: matcher), lhsNickname, rhsNickname, "of nickName"))
				return Matcher.ComparisonResult(results)

            case (.m_findFollowed__by_nickName(let lhsNickname), .m_findFollowed__by_nickName(let rhsNickname)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsNickname, rhs: rhsNickname, with: matcher), lhsNickname, rhsNickname, "by nickName"))
				return Matcher.ComparisonResult(results)
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case let .m_save__follow_follow(p0): return p0.intValue
            case let .m_findFollowers__of_nickName(p0): return p0.intValue
            case let .m_findFollowed__by_nickName(p0): return p0.intValue
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_save__follow_follow: return ".save(follow:)"
            case .m_findFollowers__of_nickName: return ".findFollowers(of:)"
            case .m_findFollowed__by_nickName: return ".findFollowed(by:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


        public static func findFollowers(of nickName: Parameter<String>, willReturn: [String]...) -> MethodStub {
            return Given(method: .m_findFollowers__of_nickName(`nickName`), products: willReturn.map({ StubProduct.return($0 as Any) }))
        }
        public static func findFollowed(by nickName: Parameter<String>, willReturn: [String]...) -> MethodStub {
            return Given(method: .m_findFollowed__by_nickName(`nickName`), products: willReturn.map({ StubProduct.return($0 as Any) }))
        }
        public static func save(follow: Parameter<Follow>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_save__follow_follow(`follow`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func save(follow: Parameter<Follow>, willProduce: (StubberThrows<Void>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_save__follow_follow(`follow`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: (Void).self)
			willProduce(stubber)
			return given
        }
        public static func findFollowers(of nickName: Parameter<String>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_findFollowers__of_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func findFollowers(of nickName: Parameter<String>, willProduce: (StubberThrows<[String]>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_findFollowers__of_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: ([String]).self)
			willProduce(stubber)
			return given
        }
        public static func findFollowed(by nickName: Parameter<String>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_findFollowed__by_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func findFollowed(by nickName: Parameter<String>, willProduce: (StubberThrows<[String]>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_findFollowed__by_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: ([String]).self)
			willProduce(stubber)
			return given
        }
    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func save(follow: Parameter<Follow>) -> Verify { return Verify(method: .m_save__follow_follow(`follow`))}
        public static func findFollowers(of nickName: Parameter<String>) -> Verify { return Verify(method: .m_findFollowers__of_nickName(`nickName`))}
        public static func findFollowed(by nickName: Parameter<String>) -> Verify { return Verify(method: .m_findFollowed__by_nickName(`nickName`))}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func save(follow: Parameter<Follow>, perform: @escaping (Follow) -> Void) -> Perform {
            return Perform(method: .m_save__follow_follow(`follow`), performs: perform)
        }
        public static func findFollowers(of nickName: Parameter<String>, perform: @escaping (String) -> Void) -> Perform {
            return Perform(method: .m_findFollowers__of_nickName(`nickName`), performs: perform)
        }
        public static func findFollowed(by nickName: Parameter<String>, perform: @escaping (String) -> Void) -> Perform {
            return Perform(method: .m_findFollowed__by_nickName(`nickName`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        invocations.append(call)
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - FollowService

open class FollowServiceMock: FollowService, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func add(follow: Follow) throws {
        addInvocation(.m_add__follow_follow(Parameter<Follow>.value(`follow`)))
		let perform = methodPerformValue(.m_add__follow_follow(Parameter<Follow>.value(`follow`))) as? (Follow) -> Void
		perform?(`follow`)
		do {
		    _ = try methodReturnValue(.m_add__follow_follow(Parameter<Follow>.value(`follow`))).casted() as Void
		} catch MockError.notStubed {
			// do nothing
		} catch {
		    throw error
		}
    }

    open func followers(of nickName: String) throws -> [String] {
        addInvocation(.m_followers__of_nickName(Parameter<String>.value(`nickName`)))
		let perform = methodPerformValue(.m_followers__of_nickName(Parameter<String>.value(`nickName`))) as? (String) -> Void
		perform?(`nickName`)
		var __value: [String]
		do {
		    __value = try methodReturnValue(.m_followers__of_nickName(Parameter<String>.value(`nickName`))).casted()
		} catch MockError.notStubed {
			onFatalFailure("Stub return value not specified for followers(of nickName: String). Use given")
			Failure("Stub return value not specified for followers(of nickName: String). Use given")
		} catch {
		    throw error
		}
		return __value
    }

    open func followed(by nickName: String) throws -> [String] {
        addInvocation(.m_followed__by_nickName(Parameter<String>.value(`nickName`)))
		let perform = methodPerformValue(.m_followed__by_nickName(Parameter<String>.value(`nickName`))) as? (String) -> Void
		perform?(`nickName`)
		var __value: [String]
		do {
		    __value = try methodReturnValue(.m_followed__by_nickName(Parameter<String>.value(`nickName`))).casted()
		} catch MockError.notStubed {
			onFatalFailure("Stub return value not specified for followed(by nickName: String). Use given")
			Failure("Stub return value not specified for followed(by nickName: String). Use given")
		} catch {
		    throw error
		}
		return __value
    }


    fileprivate enum MethodType {
        case m_add__follow_follow(Parameter<Follow>)
        case m_followers__of_nickName(Parameter<String>)
        case m_followed__by_nickName(Parameter<String>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_add__follow_follow(let lhsFollow), .m_add__follow_follow(let rhsFollow)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsFollow, rhs: rhsFollow, with: matcher), lhsFollow, rhsFollow, "follow"))
				return Matcher.ComparisonResult(results)

            case (.m_followers__of_nickName(let lhsNickname), .m_followers__of_nickName(let rhsNickname)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsNickname, rhs: rhsNickname, with: matcher), lhsNickname, rhsNickname, "of nickName"))
				return Matcher.ComparisonResult(results)

            case (.m_followed__by_nickName(let lhsNickname), .m_followed__by_nickName(let rhsNickname)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsNickname, rhs: rhsNickname, with: matcher), lhsNickname, rhsNickname, "by nickName"))
				return Matcher.ComparisonResult(results)
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case let .m_add__follow_follow(p0): return p0.intValue
            case let .m_followers__of_nickName(p0): return p0.intValue
            case let .m_followed__by_nickName(p0): return p0.intValue
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_add__follow_follow: return ".add(follow:)"
            case .m_followers__of_nickName: return ".followers(of:)"
            case .m_followed__by_nickName: return ".followed(by:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


        public static func followers(of nickName: Parameter<String>, willReturn: [String]...) -> MethodStub {
            return Given(method: .m_followers__of_nickName(`nickName`), products: willReturn.map({ StubProduct.return($0 as Any) }))
        }
        public static func followed(by nickName: Parameter<String>, willReturn: [String]...) -> MethodStub {
            return Given(method: .m_followed__by_nickName(`nickName`), products: willReturn.map({ StubProduct.return($0 as Any) }))
        }
        public static func add(follow: Parameter<Follow>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_add__follow_follow(`follow`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func add(follow: Parameter<Follow>, willProduce: (StubberThrows<Void>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_add__follow_follow(`follow`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: (Void).self)
			willProduce(stubber)
			return given
        }
        public static func followers(of nickName: Parameter<String>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_followers__of_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func followers(of nickName: Parameter<String>, willProduce: (StubberThrows<[String]>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_followers__of_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: ([String]).self)
			willProduce(stubber)
			return given
        }
        public static func followed(by nickName: Parameter<String>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_followed__by_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func followed(by nickName: Parameter<String>, willProduce: (StubberThrows<[String]>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_followed__by_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: ([String]).self)
			willProduce(stubber)
			return given
        }
    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func add(follow: Parameter<Follow>) -> Verify { return Verify(method: .m_add__follow_follow(`follow`))}
        public static func followers(of nickName: Parameter<String>) -> Verify { return Verify(method: .m_followers__of_nickName(`nickName`))}
        public static func followed(by nickName: Parameter<String>) -> Verify { return Verify(method: .m_followed__by_nickName(`nickName`))}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func add(follow: Parameter<Follow>, perform: @escaping (Follow) -> Void) -> Perform {
            return Perform(method: .m_add__follow_follow(`follow`), performs: perform)
        }
        public static func followers(of nickName: Parameter<String>, perform: @escaping (String) -> Void) -> Perform {
            return Perform(method: .m_followers__of_nickName(`nickName`), performs: perform)
        }
        public static func followed(by nickName: Parameter<String>, perform: @escaping (String) -> Void) -> Perform {
            return Perform(method: .m_followed__by_nickName(`nickName`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        invocations.append(call)
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - TweetIdGenerator

open class TweetIdGeneratorMock: TweetIdGenerator, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func callAsFunction() -> TweetID {
        addInvocation(.m_callAsFunction)
		let perform = methodPerformValue(.m_callAsFunction) as? () -> Void
		perform?()
		var __value: TweetID
		do {
		    __value = try methodReturnValue(.m_callAsFunction).casted()
		} catch {
			onFatalFailure("Stub return value not specified for callAsFunction(). Use given")
			Failure("Stub return value not specified for callAsFunction(). Use given")
		}
		return __value
    }


    fileprivate enum MethodType {
        case m_callAsFunction

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_callAsFunction, .m_callAsFunction): return .match
            }
        }

        func intValue() -> Int {
            switch self {
            case .m_callAsFunction: return 0
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_callAsFunction: return ".callAsFunction()"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


        public static func callAsFunction(willReturn: TweetID...) -> MethodStub {
            return Given(method: .m_callAsFunction, products: willReturn.map({ StubProduct.return($0 as Any) }))
        }
        public static func callAsFunction(willProduce: (Stubber<TweetID>) -> Void) -> MethodStub {
            let willReturn: [TweetID] = []
			let given: Given = { return Given(method: .m_callAsFunction, products: willReturn.map({ StubProduct.return($0 as Any) })) }()
			let stubber = given.stub(for: (TweetID).self)
			willProduce(stubber)
			return given
        }
    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func callAsFunction() -> Verify { return Verify(method: .m_callAsFunction)}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func callAsFunction(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_callAsFunction, performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        invocations.append(call)
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - TweetRepository

open class TweetRepositoryMock: TweetRepository, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func save(tweet: Tweet) throws {
        addInvocation(.m_save__tweet_tweet(Parameter<Tweet>.value(`tweet`)))
		let perform = methodPerformValue(.m_save__tweet_tweet(Parameter<Tweet>.value(`tweet`))) as? (Tweet) -> Void
		perform?(`tweet`)
		do {
		    _ = try methodReturnValue(.m_save__tweet_tweet(Parameter<Tweet>.value(`tweet`))).casted() as Void
		} catch MockError.notStubed {
			// do nothing
		} catch {
		    throw error
		}
    }

    open func findTweets(of nickName: String) throws -> [Tweet] {
        addInvocation(.m_findTweets__of_nickName(Parameter<String>.value(`nickName`)))
		let perform = methodPerformValue(.m_findTweets__of_nickName(Parameter<String>.value(`nickName`))) as? (String) -> Void
		perform?(`nickName`)
		var __value: [Tweet]
		do {
		    __value = try methodReturnValue(.m_findTweets__of_nickName(Parameter<String>.value(`nickName`))).casted()
		} catch MockError.notStubed {
			onFatalFailure("Stub return value not specified for findTweets(of nickName: String). Use given")
			Failure("Stub return value not specified for findTweets(of nickName: String). Use given")
		} catch {
		    throw error
		}
		return __value
    }


    fileprivate enum MethodType {
        case m_save__tweet_tweet(Parameter<Tweet>)
        case m_findTweets__of_nickName(Parameter<String>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_save__tweet_tweet(let lhsTweet), .m_save__tweet_tweet(let rhsTweet)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsTweet, rhs: rhsTweet, with: matcher), lhsTweet, rhsTweet, "tweet"))
				return Matcher.ComparisonResult(results)

            case (.m_findTweets__of_nickName(let lhsNickname), .m_findTweets__of_nickName(let rhsNickname)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsNickname, rhs: rhsNickname, with: matcher), lhsNickname, rhsNickname, "of nickName"))
				return Matcher.ComparisonResult(results)
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case let .m_save__tweet_tweet(p0): return p0.intValue
            case let .m_findTweets__of_nickName(p0): return p0.intValue
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_save__tweet_tweet: return ".save(tweet:)"
            case .m_findTweets__of_nickName: return ".findTweets(of:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


        public static func findTweets(of nickName: Parameter<String>, willReturn: [Tweet]...) -> MethodStub {
            return Given(method: .m_findTweets__of_nickName(`nickName`), products: willReturn.map({ StubProduct.return($0 as Any) }))
        }
        public static func save(tweet: Parameter<Tweet>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_save__tweet_tweet(`tweet`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func save(tweet: Parameter<Tweet>, willProduce: (StubberThrows<Void>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_save__tweet_tweet(`tweet`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: (Void).self)
			willProduce(stubber)
			return given
        }
        public static func findTweets(of nickName: Parameter<String>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_findTweets__of_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func findTweets(of nickName: Parameter<String>, willProduce: (StubberThrows<[Tweet]>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_findTweets__of_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: ([Tweet]).self)
			willProduce(stubber)
			return given
        }
    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func save(tweet: Parameter<Tweet>) -> Verify { return Verify(method: .m_save__tweet_tweet(`tweet`))}
        public static func findTweets(of nickName: Parameter<String>) -> Verify { return Verify(method: .m_findTweets__of_nickName(`nickName`))}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func save(tweet: Parameter<Tweet>, perform: @escaping (Tweet) -> Void) -> Perform {
            return Perform(method: .m_save__tweet_tweet(`tweet`), performs: perform)
        }
        public static func findTweets(of nickName: Parameter<String>, perform: @escaping (String) -> Void) -> Perform {
            return Perform(method: .m_findTweets__of_nickName(`nickName`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        invocations.append(call)
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - TweetService

open class TweetServiceMock: TweetService, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func createTweet(createTweetData: CreateTweetData) throws {
        addInvocation(.m_createTweet__createTweetData_createTweetData(Parameter<CreateTweetData>.value(`createTweetData`)))
		let perform = methodPerformValue(.m_createTweet__createTweetData_createTweetData(Parameter<CreateTweetData>.value(`createTweetData`))) as? (CreateTweetData) -> Void
		perform?(`createTweetData`)
		do {
		    _ = try methodReturnValue(.m_createTweet__createTweetData_createTweetData(Parameter<CreateTweetData>.value(`createTweetData`))).casted() as Void
		} catch MockError.notStubed {
			// do nothing
		} catch {
		    throw error
		}
    }

    open func getTweets(of nickName: String) throws -> [Tweet] {
        addInvocation(.m_getTweets__of_nickName(Parameter<String>.value(`nickName`)))
		let perform = methodPerformValue(.m_getTweets__of_nickName(Parameter<String>.value(`nickName`))) as? (String) -> Void
		perform?(`nickName`)
		var __value: [Tweet]
		do {
		    __value = try methodReturnValue(.m_getTweets__of_nickName(Parameter<String>.value(`nickName`))).casted()
		} catch MockError.notStubed {
			onFatalFailure("Stub return value not specified for getTweets(of nickName: String). Use given")
			Failure("Stub return value not specified for getTweets(of nickName: String). Use given")
		} catch {
		    throw error
		}
		return __value
    }


    fileprivate enum MethodType {
        case m_createTweet__createTweetData_createTweetData(Parameter<CreateTweetData>)
        case m_getTweets__of_nickName(Parameter<String>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_createTweet__createTweetData_createTweetData(let lhsCreatetweetdata), .m_createTweet__createTweetData_createTweetData(let rhsCreatetweetdata)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsCreatetweetdata, rhs: rhsCreatetweetdata, with: matcher), lhsCreatetweetdata, rhsCreatetweetdata, "createTweetData"))
				return Matcher.ComparisonResult(results)

            case (.m_getTweets__of_nickName(let lhsNickname), .m_getTweets__of_nickName(let rhsNickname)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsNickname, rhs: rhsNickname, with: matcher), lhsNickname, rhsNickname, "of nickName"))
				return Matcher.ComparisonResult(results)
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case let .m_createTweet__createTweetData_createTweetData(p0): return p0.intValue
            case let .m_getTweets__of_nickName(p0): return p0.intValue
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_createTweet__createTweetData_createTweetData: return ".createTweet(createTweetData:)"
            case .m_getTweets__of_nickName: return ".getTweets(of:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


        public static func getTweets(of nickName: Parameter<String>, willReturn: [Tweet]...) -> MethodStub {
            return Given(method: .m_getTweets__of_nickName(`nickName`), products: willReturn.map({ StubProduct.return($0 as Any) }))
        }
        public static func createTweet(createTweetData: Parameter<CreateTweetData>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_createTweet__createTweetData_createTweetData(`createTweetData`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func createTweet(createTweetData: Parameter<CreateTweetData>, willProduce: (StubberThrows<Void>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_createTweet__createTweetData_createTweetData(`createTweetData`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: (Void).self)
			willProduce(stubber)
			return given
        }
        public static func getTweets(of nickName: Parameter<String>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_getTweets__of_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func getTweets(of nickName: Parameter<String>, willProduce: (StubberThrows<[Tweet]>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_getTweets__of_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: ([Tweet]).self)
			willProduce(stubber)
			return given
        }
    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func createTweet(createTweetData: Parameter<CreateTweetData>) -> Verify { return Verify(method: .m_createTweet__createTweetData_createTweetData(`createTweetData`))}
        public static func getTweets(of nickName: Parameter<String>) -> Verify { return Verify(method: .m_getTweets__of_nickName(`nickName`))}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func createTweet(createTweetData: Parameter<CreateTweetData>, perform: @escaping (CreateTweetData) -> Void) -> Perform {
            return Perform(method: .m_createTweet__createTweetData_createTweetData(`createTweetData`), performs: perform)
        }
        public static func getTweets(of nickName: Parameter<String>, perform: @escaping (String) -> Void) -> Perform {
            return Perform(method: .m_getTweets__of_nickName(`nickName`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        invocations.append(call)
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - UserRepository

open class UserRepositoryMock: UserRepository, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func exists(nickName: String) -> Bool {
        addInvocation(.m_exists__nickName_nickName(Parameter<String>.value(`nickName`)))
		let perform = methodPerformValue(.m_exists__nickName_nickName(Parameter<String>.value(`nickName`))) as? (String) -> Void
		perform?(`nickName`)
		var __value: Bool
		do {
		    __value = try methodReturnValue(.m_exists__nickName_nickName(Parameter<String>.value(`nickName`))).casted()
		} catch {
			onFatalFailure("Stub return value not specified for exists(nickName: String). Use given")
			Failure("Stub return value not specified for exists(nickName: String). Use given")
		}
		return __value
    }

    open func save(user: User) throws {
        addInvocation(.m_save__user_user(Parameter<User>.value(`user`)))
		let perform = methodPerformValue(.m_save__user_user(Parameter<User>.value(`user`))) as? (User) -> Void
		perform?(`user`)
		do {
		    _ = try methodReturnValue(.m_save__user_user(Parameter<User>.value(`user`))).casted() as Void
		} catch MockError.notStubed {
			// do nothing
		} catch {
		    throw error
		}
    }

    open func findBy(nickName: String) throws -> User {
        addInvocation(.m_findBy__nickName_nickName(Parameter<String>.value(`nickName`)))
		let perform = methodPerformValue(.m_findBy__nickName_nickName(Parameter<String>.value(`nickName`))) as? (String) -> Void
		perform?(`nickName`)
		var __value: User
		do {
		    __value = try methodReturnValue(.m_findBy__nickName_nickName(Parameter<String>.value(`nickName`))).casted()
		} catch MockError.notStubed {
			onFatalFailure("Stub return value not specified for findBy(nickName: String). Use given")
			Failure("Stub return value not specified for findBy(nickName: String). Use given")
		} catch {
		    throw error
		}
		return __value
    }

    open func update(user: User) throws {
        addInvocation(.m_update__user_user(Parameter<User>.value(`user`)))
		let perform = methodPerformValue(.m_update__user_user(Parameter<User>.value(`user`))) as? (User) -> Void
		perform?(`user`)
		do {
		    _ = try methodReturnValue(.m_update__user_user(Parameter<User>.value(`user`))).casted() as Void
		} catch MockError.notStubed {
			// do nothing
		} catch {
		    throw error
		}
    }


    fileprivate enum MethodType {
        case m_exists__nickName_nickName(Parameter<String>)
        case m_save__user_user(Parameter<User>)
        case m_findBy__nickName_nickName(Parameter<String>)
        case m_update__user_user(Parameter<User>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_exists__nickName_nickName(let lhsNickname), .m_exists__nickName_nickName(let rhsNickname)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsNickname, rhs: rhsNickname, with: matcher), lhsNickname, rhsNickname, "nickName"))
				return Matcher.ComparisonResult(results)

            case (.m_save__user_user(let lhsUser), .m_save__user_user(let rhsUser)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsUser, rhs: rhsUser, with: matcher), lhsUser, rhsUser, "user"))
				return Matcher.ComparisonResult(results)

            case (.m_findBy__nickName_nickName(let lhsNickname), .m_findBy__nickName_nickName(let rhsNickname)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsNickname, rhs: rhsNickname, with: matcher), lhsNickname, rhsNickname, "nickName"))
				return Matcher.ComparisonResult(results)

            case (.m_update__user_user(let lhsUser), .m_update__user_user(let rhsUser)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsUser, rhs: rhsUser, with: matcher), lhsUser, rhsUser, "user"))
				return Matcher.ComparisonResult(results)
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case let .m_exists__nickName_nickName(p0): return p0.intValue
            case let .m_save__user_user(p0): return p0.intValue
            case let .m_findBy__nickName_nickName(p0): return p0.intValue
            case let .m_update__user_user(p0): return p0.intValue
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_exists__nickName_nickName: return ".exists(nickName:)"
            case .m_save__user_user: return ".save(user:)"
            case .m_findBy__nickName_nickName: return ".findBy(nickName:)"
            case .m_update__user_user: return ".update(user:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


        public static func exists(nickName: Parameter<String>, willReturn: Bool...) -> MethodStub {
            return Given(method: .m_exists__nickName_nickName(`nickName`), products: willReturn.map({ StubProduct.return($0 as Any) }))
        }
        public static func findBy(nickName: Parameter<String>, willReturn: User...) -> MethodStub {
            return Given(method: .m_findBy__nickName_nickName(`nickName`), products: willReturn.map({ StubProduct.return($0 as Any) }))
        }
        public static func exists(nickName: Parameter<String>, willProduce: (Stubber<Bool>) -> Void) -> MethodStub {
            let willReturn: [Bool] = []
			let given: Given = { return Given(method: .m_exists__nickName_nickName(`nickName`), products: willReturn.map({ StubProduct.return($0 as Any) })) }()
			let stubber = given.stub(for: (Bool).self)
			willProduce(stubber)
			return given
        }
        public static func save(user: Parameter<User>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_save__user_user(`user`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func save(user: Parameter<User>, willProduce: (StubberThrows<Void>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_save__user_user(`user`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: (Void).self)
			willProduce(stubber)
			return given
        }
        public static func findBy(nickName: Parameter<String>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_findBy__nickName_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func findBy(nickName: Parameter<String>, willProduce: (StubberThrows<User>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_findBy__nickName_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: (User).self)
			willProduce(stubber)
			return given
        }
        public static func update(user: Parameter<User>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_update__user_user(`user`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func update(user: Parameter<User>, willProduce: (StubberThrows<Void>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_update__user_user(`user`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: (Void).self)
			willProduce(stubber)
			return given
        }
    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func exists(nickName: Parameter<String>) -> Verify { return Verify(method: .m_exists__nickName_nickName(`nickName`))}
        public static func save(user: Parameter<User>) -> Verify { return Verify(method: .m_save__user_user(`user`))}
        public static func findBy(nickName: Parameter<String>) -> Verify { return Verify(method: .m_findBy__nickName_nickName(`nickName`))}
        public static func update(user: Parameter<User>) -> Verify { return Verify(method: .m_update__user_user(`user`))}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func exists(nickName: Parameter<String>, perform: @escaping (String) -> Void) -> Perform {
            return Perform(method: .m_exists__nickName_nickName(`nickName`), performs: perform)
        }
        public static func save(user: Parameter<User>, perform: @escaping (User) -> Void) -> Perform {
            return Perform(method: .m_save__user_user(`user`), performs: perform)
        }
        public static func findBy(nickName: Parameter<String>, perform: @escaping (String) -> Void) -> Perform {
            return Perform(method: .m_findBy__nickName_nickName(`nickName`), performs: perform)
        }
        public static func update(user: Parameter<User>, perform: @escaping (User) -> Void) -> Perform {
            return Perform(method: .m_update__user_user(`user`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        invocations.append(call)
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - UserService

open class UserServiceMock: UserService, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func exists(nickName: String) -> Bool {
        addInvocation(.m_exists__nickName_nickName(Parameter<String>.value(`nickName`)))
		let perform = methodPerformValue(.m_exists__nickName_nickName(Parameter<String>.value(`nickName`))) as? (String) -> Void
		perform?(`nickName`)
		var __value: Bool
		do {
		    __value = try methodReturnValue(.m_exists__nickName_nickName(Parameter<String>.value(`nickName`))).casted()
		} catch {
			onFatalFailure("Stub return value not specified for exists(nickName: String). Use given")
			Failure("Stub return value not specified for exists(nickName: String). Use given")
		}
		return __value
    }

    open func add(user: User) throws {
        addInvocation(.m_add__user_user(Parameter<User>.value(`user`)))
		let perform = methodPerformValue(.m_add__user_user(Parameter<User>.value(`user`))) as? (User) -> Void
		perform?(`user`)
		do {
		    _ = try methodReturnValue(.m_add__user_user(Parameter<User>.value(`user`))).casted() as Void
		} catch MockError.notStubed {
			// do nothing
		} catch {
		    throw error
		}
    }

    open func findBy(nickName: String) throws -> User {
        addInvocation(.m_findBy__nickName_nickName(Parameter<String>.value(`nickName`)))
		let perform = methodPerformValue(.m_findBy__nickName_nickName(Parameter<String>.value(`nickName`))) as? (String) -> Void
		perform?(`nickName`)
		var __value: User
		do {
		    __value = try methodReturnValue(.m_findBy__nickName_nickName(Parameter<String>.value(`nickName`))).casted()
		} catch MockError.notStubed {
			onFatalFailure("Stub return value not specified for findBy(nickName: String). Use given")
			Failure("Stub return value not specified for findBy(nickName: String). Use given")
		} catch {
		    throw error
		}
		return __value
    }

    open func updateRealName(nickName: String, updatedRealName: String) throws {
        addInvocation(.m_updateRealName__nickName_nickNameupdatedRealName_updatedRealName(Parameter<String>.value(`nickName`), Parameter<String>.value(`updatedRealName`)))
		let perform = methodPerformValue(.m_updateRealName__nickName_nickNameupdatedRealName_updatedRealName(Parameter<String>.value(`nickName`), Parameter<String>.value(`updatedRealName`))) as? (String, String) -> Void
		perform?(`nickName`, `updatedRealName`)
		do {
		    _ = try methodReturnValue(.m_updateRealName__nickName_nickNameupdatedRealName_updatedRealName(Parameter<String>.value(`nickName`), Parameter<String>.value(`updatedRealName`))).casted() as Void
		} catch MockError.notStubed {
			// do nothing
		} catch {
		    throw error
		}
    }


    fileprivate enum MethodType {
        case m_exists__nickName_nickName(Parameter<String>)
        case m_add__user_user(Parameter<User>)
        case m_findBy__nickName_nickName(Parameter<String>)
        case m_updateRealName__nickName_nickNameupdatedRealName_updatedRealName(Parameter<String>, Parameter<String>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_exists__nickName_nickName(let lhsNickname), .m_exists__nickName_nickName(let rhsNickname)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsNickname, rhs: rhsNickname, with: matcher), lhsNickname, rhsNickname, "nickName"))
				return Matcher.ComparisonResult(results)

            case (.m_add__user_user(let lhsUser), .m_add__user_user(let rhsUser)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsUser, rhs: rhsUser, with: matcher), lhsUser, rhsUser, "user"))
				return Matcher.ComparisonResult(results)

            case (.m_findBy__nickName_nickName(let lhsNickname), .m_findBy__nickName_nickName(let rhsNickname)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsNickname, rhs: rhsNickname, with: matcher), lhsNickname, rhsNickname, "nickName"))
				return Matcher.ComparisonResult(results)

            case (.m_updateRealName__nickName_nickNameupdatedRealName_updatedRealName(let lhsNickname, let lhsUpdatedrealname), .m_updateRealName__nickName_nickNameupdatedRealName_updatedRealName(let rhsNickname, let rhsUpdatedrealname)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsNickname, rhs: rhsNickname, with: matcher), lhsNickname, rhsNickname, "nickName"))
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsUpdatedrealname, rhs: rhsUpdatedrealname, with: matcher), lhsUpdatedrealname, rhsUpdatedrealname, "updatedRealName"))
				return Matcher.ComparisonResult(results)
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case let .m_exists__nickName_nickName(p0): return p0.intValue
            case let .m_add__user_user(p0): return p0.intValue
            case let .m_findBy__nickName_nickName(p0): return p0.intValue
            case let .m_updateRealName__nickName_nickNameupdatedRealName_updatedRealName(p0, p1): return p0.intValue + p1.intValue
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_exists__nickName_nickName: return ".exists(nickName:)"
            case .m_add__user_user: return ".add(user:)"
            case .m_findBy__nickName_nickName: return ".findBy(nickName:)"
            case .m_updateRealName__nickName_nickNameupdatedRealName_updatedRealName: return ".updateRealName(nickName:updatedRealName:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


        public static func exists(nickName: Parameter<String>, willReturn: Bool...) -> MethodStub {
            return Given(method: .m_exists__nickName_nickName(`nickName`), products: willReturn.map({ StubProduct.return($0 as Any) }))
        }
        public static func findBy(nickName: Parameter<String>, willReturn: User...) -> MethodStub {
            return Given(method: .m_findBy__nickName_nickName(`nickName`), products: willReturn.map({ StubProduct.return($0 as Any) }))
        }
        public static func exists(nickName: Parameter<String>, willProduce: (Stubber<Bool>) -> Void) -> MethodStub {
            let willReturn: [Bool] = []
			let given: Given = { return Given(method: .m_exists__nickName_nickName(`nickName`), products: willReturn.map({ StubProduct.return($0 as Any) })) }()
			let stubber = given.stub(for: (Bool).self)
			willProduce(stubber)
			return given
        }
        public static func add(user: Parameter<User>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_add__user_user(`user`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func add(user: Parameter<User>, willProduce: (StubberThrows<Void>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_add__user_user(`user`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: (Void).self)
			willProduce(stubber)
			return given
        }
        public static func findBy(nickName: Parameter<String>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_findBy__nickName_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func findBy(nickName: Parameter<String>, willProduce: (StubberThrows<User>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_findBy__nickName_nickName(`nickName`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: (User).self)
			willProduce(stubber)
			return given
        }
        public static func updateRealName(nickName: Parameter<String>, updatedRealName: Parameter<String>, willThrow: Error...) -> MethodStub {
            return Given(method: .m_updateRealName__nickName_nickNameupdatedRealName_updatedRealName(`nickName`, `updatedRealName`), products: willThrow.map({ StubProduct.throw($0) }))
        }
        public static func updateRealName(nickName: Parameter<String>, updatedRealName: Parameter<String>, willProduce: (StubberThrows<Void>) -> Void) -> MethodStub {
            let willThrow: [Error] = []
			let given: Given = { return Given(method: .m_updateRealName__nickName_nickNameupdatedRealName_updatedRealName(`nickName`, `updatedRealName`), products: willThrow.map({ StubProduct.throw($0) })) }()
			let stubber = given.stubThrows(for: (Void).self)
			willProduce(stubber)
			return given
        }
    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func exists(nickName: Parameter<String>) -> Verify { return Verify(method: .m_exists__nickName_nickName(`nickName`))}
        public static func add(user: Parameter<User>) -> Verify { return Verify(method: .m_add__user_user(`user`))}
        public static func findBy(nickName: Parameter<String>) -> Verify { return Verify(method: .m_findBy__nickName_nickName(`nickName`))}
        public static func updateRealName(nickName: Parameter<String>, updatedRealName: Parameter<String>) -> Verify { return Verify(method: .m_updateRealName__nickName_nickNameupdatedRealName_updatedRealName(`nickName`, `updatedRealName`))}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func exists(nickName: Parameter<String>, perform: @escaping (String) -> Void) -> Perform {
            return Perform(method: .m_exists__nickName_nickName(`nickName`), performs: perform)
        }
        public static func add(user: Parameter<User>, perform: @escaping (User) -> Void) -> Perform {
            return Perform(method: .m_add__user_user(`user`), performs: perform)
        }
        public static func findBy(nickName: Parameter<String>, perform: @escaping (String) -> Void) -> Perform {
            return Perform(method: .m_findBy__nickName_nickName(`nickName`), performs: perform)
        }
        public static func updateRealName(nickName: Parameter<String>, updatedRealName: Parameter<String>, perform: @escaping (String, String) -> Void) -> Perform {
            return Perform(method: .m_updateRealName__nickName_nickNameupdatedRealName_updatedRealName(`nickName`, `updatedRealName`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        invocations.append(call)
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

