#!/bin/bash
printf "\n\n> curl -X POST http://localhost:8080/user/create -H 'Content-Type: application/json' -d '{ \"nickName\":\"acid\", \"realName\":\"Ariel Cid\" }'\n"
curl -X POST http://localhost:8080/user/create -H 'Content-Type: application/json' -d '{ "nickName":"acid", "realName":"Ariel Cid" }'

printf "\n\n> curl -X POST http://localhost:8080/user/create -H 'Content-Type: application/json' -d '{ \"nickName\":\"Quique\", \"realName\":\"Enrique Nieloud\" }'\n"
curl -X POST http://localhost:8080/user/create -H 'Content-Type: application/json' -d '{ "nickName":"Quique", "realName":"Enrique Nieloud" }'

printf "\n\n> curl -X POST http://localhost:8080/follow/create -H 'Content-Type: application/json' -d '{ \"fromNickName\":\"acid\", \"toNickName\":\"Quique\" }'\n"
curl -X POST http://localhost:8080/follow/create -H 'Content-Type: application/json' -d '{ "fromNickName":"acid", "toNickName":"Quique" }'

printf "\n\n> curl -X GET http://localhost:8080/followers/get -H 'Content-Type: application/json' -d '{ \"nickName\":\"acid\" }'\n"
curl -X GET http://localhost:8080/followers/get -H 'Content-Type: application/json' -d '{ "nickName":"acid" }'

printf "\n\n> curl -X GET http://localhost:8080/followers/get -H 'Content-Type: application/json' -d '{ \"nickName\":\"Quique\" }'\n"
curl -X GET http://localhost:8080/followers/get -H 'Content-Type: application/json' -d '{ "nickName":"Quique" }'

printf "\n\n> curl -X GET http://localhost:8080/followed/get -H 'Content-Type: application/json' -d '{ \"nickName\":\"acid\" }'\n"
curl -X GET http://localhost:8080/followed/get -H 'Content-Type: application/json' -d '{ "nickName":"acid" }'

printf "\n\n> curl -X POST http://localhost:8080/tweet/create -H 'Content-Type: application/json' -d '{ \"nickName\":\"acid\", \"tweetMessage\":\"Hello!\" }'\n"
curl -X POST http://localhost:8080/tweet/create -H 'Content-Type: application/json' -d '{ "nickName":"acid", "tweetMessage":"Hello!" }'

printf "\n\n> curl -X GET http://localhost:8080/tweets/get -H 'Content-Type: application/json' -d '{ \"nickName\":\"acid\" }'\n"
curl -X GET http://localhost:8080/tweets/get -H 'Content-Type: application/json' -d '{ "nickName":"acid" }'
